# -*- encoding: utf-8 -*-
import dramatiq

from django.conf import settings
from easy_thumbnails.files import generate_all_aliases

from block.models import Image


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def thumbnail_image(image_pk):
    try:
        img = Image.objects.get(id=image_pk)
    except Image.DoesNotExist:
        img = None
    if img:
        generate_all_aliases(img.image, include_global=True)
