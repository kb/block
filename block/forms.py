# -*- encoding: utf-8 -*-
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import format_html
from django.utils.text import slugify

from base.form_utils import (
    FileDropInput,
    RequiredFieldForm,
    set_widget_required,
)
from block.models import (
    ContentModel,
    Document,
    HeaderFooter,
    Link,
    LinkCategory,
    Menu,
    MenuItem,
    Page,
    PageSection,
    Panel,
    Section,
    Template,
    TemplateSection,
)


def _link_label_from_instance(obj):
    return format_html(
        "{} (<small>{}</small>)".format(obj.title, obj.link_type_description)
    )


def _link_label_from_many_to_many_instance(obj):
    return format_html(
        "{} (<small>{}</small>)".format(
            obj.link.title, obj.link.link_type_description
        )
    )


class LinkCategoryEmptyForm(forms.ModelForm):
    class Meta:
        model = LinkCategory
        fields = ()


class LinkModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return _link_label_from_instance(obj)


class LinkModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return _link_label_from_instance(obj)


class LinkManyToManyMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return _link_label_from_many_to_many_instance(obj)


class ContentBaseForm(RequiredFieldForm):
    """Base of all block content forms.

    Inherits from RequiredFieldForm because form inheritance in django seems
    to work best there is only a single inheritance branch.

    If your form expects to receive a field list it should pop this from
    kwargs before calling super().__init__(*args, **kwargs)

    Functionality provided::

      - processes the field list from the page section

    Sample usage:

      class ExampleForm(ContentBaseForm):

          def __init__(self, *args, **kwargs):

              section_field_list = kwargs.pop("field_list", None)
              super().__init__(*args, **kwargs)

              # ... do any processing required on the fields

              self._setup_fields(section_field_list)

          class Meta:
              model = Example
              fields = ("field_1", "field_2",)
    """

    def _setup_fields(self, section_fields):
        """Remove fields not required for this section.

        This should be called at the end of the __init__ method to remove
        fields not required.
        """

        if section_fields:
            for field_name in list(self.fields):
                if field_name not in section_fields:
                    del self.fields[field_name]


class ContentEmptyForm(forms.ModelForm):
    class Meta:
        model = ContentModel
        fields = ()


class DocumentForm(forms.ModelForm):
    """Allow the user to upload a document (for the form wizard)."""

    add_to_library = forms.BooleanField(
        help_text="tick this box to add the document to the library",
        initial=True,
        required=False,
    )
    category = forms.ModelChoiceField(
        queryset=LinkCategory.objects.categories(), required=False
    )

    def __init__(self, *args, **kwargs):
        """Option to remove category and title from the form."""
        has_category = kwargs.pop("has_category", True)
        has_title = kwargs.pop("has_title", True)
        in_library = kwargs.pop("in_library", None)
        super().__init__(*args, **kwargs)
        for field_name in ["document", "title"]:
            field = self.fields[field_name]
            field.widget.attrs.update({"class": "pure-input-2-3"})
            if field_name == "title" or not in_library:
                set_widget_required(field)
        if has_category:
            category = self.fields["category"]
            category.widget.attrs.update({"class": "pure-input-2-3"})
        else:
            del self.fields["category"]
        if not has_title:
            del self.fields["title"]
        if in_library:
            del self.fields["add_to_library"]

    class Meta:
        model = Document
        fields = ("document", "title", "category", "add_to_library")
        widgets = {"document": FileDropInput}


class EmptyForm(forms.Form):
    class Meta:
        fields = ()


class ExternalLinkForm(forms.ModelForm):
    """Enter a URL for a web site (for the form wizard)."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("title", "url_external"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Link
        fields = ("title", "url_external", "category")


class HeaderFooterForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        field_names = (
            "header",
            "footer_left",
            "footer_right",
            "url_facebook",
            "url_linkedin",
            "url_twitter",
        )
        for name in field_names:
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = HeaderFooter
        fields = (
            "header",
            "footer_left",
            "footer_right",
            "url_facebook",
            "url_linkedin",
            "url_twitter",
        )


class LinkCategoryForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = LinkCategory
        fields = ("name",)


class LinkEmptyForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = ()


class LinkListForm(forms.Form):
    """List of links (for the form wizard)."""

    links = LinkModelChoiceField(
        queryset=Link.objects.links(),
        empty_label=None,
        widget=forms.RadioSelect,
    )

    def __init__(self, *args, **kwargs):
        category_slug = kwargs.pop("category_slug")
        super().__init__(*args, **kwargs)
        if category_slug:
            links = self.fields["links"]
            links.queryset = Link.objects.links().filter(
                category__slug=category_slug
            )

    class Meta:
        model = Link
        fields = ("links",)


class LinkMultiSelectForm(forms.Form):
    """List of links (for the form wizard)."""

    links = LinkModelMultipleChoiceField(
        queryset=Link.objects.links(), widget=forms.CheckboxSelectMultiple
    )

    def __init__(self, *args, **kwargs):
        category_slug = kwargs.pop("category_slug")
        super().__init__(*args, **kwargs)
        if category_slug:
            links = self.fields["links"]
            links.queryset = Link.objects.links().filter(
                category__slug=category_slug
            )

    class Meta:
        fields = ("links",)


class LinkSelectForm(forms.Form):
    """List of current images in the slideshow."""

    # Note: The ``queryset`` will not contain ``Link`` records.
    many_to_many = LinkManyToManyMultipleChoiceField(
        queryset=Link.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, *args, **kwargs):
        qs_many_to_many = kwargs.pop("many_to_many")
        super().__init__(*args, **kwargs)
        many_to_many = self.fields["many_to_many"]
        many_to_many.queryset = qs_many_to_many.order_by("order")
        # tick every link - so the user can untick the ones they want to remove
        initial = {item.pk: True for item in qs_many_to_many}
        many_to_many.initial = initial


class MenuItemEmptyForm(forms.ModelForm):
    class Meta:
        model = MenuItem
        fields = ()


class MenuItemBaseForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        this_pk = kwargs.pop("this_pk", None)
        self.menu = kwargs.pop("menu", Menu.objects.navigation_menu())
        super().__init__(*args, **kwargs)
        for name in ("order", "title", "parent"):
            field = self.fields[name]
            field.widget.attrs.update({"class": "pure-input-1", "rows": 2})
        self.fields["parent"].queryset = MenuItem.objects.parent_list(
            self.menu, this_pk
        )


class MenuItemForm(MenuItemBaseForm):
    class Meta:
        model = MenuItem
        fields = ("order", "title", "parent")

    def clean(self):
        cleaned_data = super().clean()
        title = cleaned_data.get("title")
        slug = slugify(title)
        qs = MenuItem.objects.filter(menu=self.menu, slug=slug)
        if self.instance.pk:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            raise forms.ValidationError(
                "A menu item with a slug of '{}' already exists "
                "on the {}".format(slug, self.menu.title),
                code="menuitem__slug__exists",
            )
        return cleaned_data


class PageEmptyForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = ()


class PageBaseForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        template_category = kwargs.pop("template_category", None)
        super().__init__(*args, **kwargs)
        for name in ("meta_description", "meta_keywords"):
            field = self.fields[name]
            field.widget.attrs.update({"class": "pure-input-1", "rows": 2})
        template = self.fields["template"]
        template.queryset = Template.objects.templates(template_category)


class PageForm(PageBaseForm):
    class Meta:
        model = Page
        fields = (
            "name",
            "slug",
            "slug_menu",
            "order",
            "meta_description",
            "meta_keywords",
            "is_home",
            "template",
        )


class PageFormSimple(PageBaseForm):
    class Meta:
        model = Page
        fields = ("name", "template", "meta_description", "meta_keywords")


class PageFormSimpleUpdate(PageBaseForm):
    class Meta:
        model = Page
        fields = (
            "name",
            "template",
            "order",
            "meta_description",
            "meta_keywords",
        )


class PageListForm(forms.ModelForm):
    """Select a page from this web site (for the form wizard)."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["url_internal"].label = "URL"

    class Meta:
        model = Link
        fields = ("title", "url_internal", "category")


class PageSectionEmptyForm(forms.ModelForm):
    class Meta:
        model = PageSection
        fields = ()


class PageSectionOperationForm(forms.ModelForm):
    operation = forms.CharField(max_length=20, widget=forms.HiddenInput())

    class Meta:
        model = PageSection
        fields = ["operation"]


class SectionForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in self.fields:
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Section
        fields = (
            "name",
            "slug",
            "block_app",
            "block_model",
            "create_url_name",
            "paginated",
            "snippet_name",
            "field_list",
        )


class PanelForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("slug", "name", "title"):
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Panel
        fields = ("slug", "name", "title")


class PanelElementForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        self.panel = kwargs.pop("panel")
        super().__init__(*args, **kwargs)
        self.fields["section"].widget.attrs.update({"class": "pure-input-1"})
        self.fields["section"].queryset = self.panel.available_sections()
        # add the page and panel to the cleaned data so that validate_unique
        # works
        self.fields["page"].widget = forms.HiddenInput()
        self.fields["page"].initial = self.panel.block.page_section.page
        self.fields["panel"].widget = forms.HiddenInput()
        self.fields["panel"].initial = self.panel.block

    class Meta:
        model = PageSection
        fields = ("section", "page", "panel")

    def clean(self):
        cleaned_data = super().clean()
        section = cleaned_data.get("section")
        try:
            PageSection.objects.get(
                page=self.panel.block.page_section.page,
                panel=self.panel.block,
                section=section,
            )
            raise forms.ValidationError(
                "A Section of this type '{}' already exists".format(
                    section.name
                )
            )
        except ObjectDoesNotExist:
            pass

        return cleaned_data


class TemplateForm(RequiredFieldForm):
    class Meta:
        model = Template
        fields = ("name", "category", "template_name")


class TemplateSectionEmptyForm(forms.ModelForm):
    class Meta:
        model = TemplateSection
        fields = ()


class TemplateSectionForm(RequiredFieldForm):
    class Meta:
        model = TemplateSection
        fields = ("section",)
