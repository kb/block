# -*- encoding: utf-8 -*-
from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.db import transaction
from django.db.models import Max
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.text import slugify
from django.views.generic.base import RedirectView
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    FormView,
    ListView,
    UpdateView,
    TemplateView,
)

from base.view_utils import BaseMixin, RedirectNextMixin
from gallery.views import WizardMixin
from login.views import RoleRequiredMixin
from .forms import (
    ContentEmptyForm,
    DocumentForm,
    EmptyForm,
    ExternalLinkForm,
    HeaderFooterForm,
    LinkCategoryEmptyForm,
    LinkCategoryForm,
    LinkEmptyForm,
    LinkListForm,
    LinkMultiSelectForm,
    LinkSelectForm,
    MenuItemEmptyForm,
    MenuItemForm,
    PageEmptyForm,
    PageForm,
    PageFormSimple,
    PageFormSimpleUpdate,
    PageListForm,
    PageSectionEmptyForm,
    PageSectionOperationForm,
    PanelElementForm,
    PanelForm,
    SectionForm,
    TemplateForm,
    TemplateSectionEmptyForm,
    TemplateSectionForm,
)
from .models import (
    BlockError,
    BlockSettings,
    HeaderFooter,
    Link,
    LinkCategory,
    Menu,
    MenuItem,
    Page,
    PageSection,
    Panel,
    PanelBlock,
    Section,
    Template,
    TemplateSection,
    Url,
    ViewUrl,
    Wizard,
)


class CMSRoleRequiredMixin(RoleRequiredMixin):
    def get_role(self):
        block_settings = BlockSettings.load()
        # return the design_role from block_settings
        return block_settings.design_role


class ContentPageMixin(BaseMixin):
    """Page information."""

    def _google_site_tag(self):
        block_settings = BlockSettings.load()
        return block_settings.google_site_tag

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                page=self.get_page(),
                menu_list=Page.objects.menu(),
                footer=self.get_footer(),
                google_site_tag=self._google_site_tag(),
            )
        )
        return context

    def get_footer(self):
        try:
            return Page.objects.get(slug=Page.CUSTOM, slug_menu=Page.FOOTER)
        except Page.DoesNotExist:
            return None

    def get_page(self):
        result = None
        menu = self.kwargs.get("menu", "")
        page = self.kwargs.get("page", "")
        if not page:
            raise BlockError("no 'page' parameter in url")
        try:
            result = Page.objects.get(slug=page, slug_menu=menu)
            if result.deleted:
                raise Http404(
                    "Page '{}', menu '{}' is deleted".format(page, menu)
                )
        except Page.DoesNotExist:
            raise Http404(
                "Page '{}', menu '{}' does not exist".format(page, menu)
            )
        return result

    def get_page_section(self):
        page = self.get_page()
        section = self.get_section()
        panel = self.get_panel()
        try:
            if panel:
                return PageSection.objects.get(
                    page=page, section=section, panel=panel
                )
            else:
                return PageSection.objects.get(
                    page=page, section=section, panel__isnull=True
                )
        except PageSection.DoesNotExist:
            raise BlockError(
                "Page section '{}/{}' does not exist".format(
                    page.slug, section.slug
                )
            )

    def get_section(self):
        section = self.kwargs.get("section", None)
        if not section:
            raise BlockError("no 'section' parameter in url")
        try:
            return Section.objects.get(slug=section)
        except Section.DoesNotExist:
            raise BlockError("Section '{}' does not exist".format(section))

    def get_panel(self):
        panel = self.kwargs.get("panel", None)
        if not panel:
            return None
        try:
            return PanelBlock.objects.get(pk=panel)
        except PanelBlock.DoesNotExist:
            raise BlockError("Panel '{}' does not exist".format(panel))

    def get_success_url(self):
        return self.object.block.page_section.page.get_design_url()


class ContentFormMixin(object):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        section = self.get_section()

        if section.field_list:
            section_field_list = section.field_list.split()
            kwargs.update({"field_list": section_field_list})
        return kwargs


class BlockMoveUpDownUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, UpdateView
):
    """
    Ideas
    =====

    - My original plan to have one view won't work because there are lots of
      block models.  I won't know which one we are trying to move!
    - But... I could use the same idea as we use for the ``_create_url``...
      No... that won't work because we won't know the ``pk`` of the block.
    - In fact.. let's use the same approach as we do for the content models
      i.e. define a ``url_update`` method...
    - Any chance we could combine these two ideas...?
      No... better to use the ``url_update`` idea because we have an object we
      are moving.
      The ``_create_url`` idea is awkward because we don't have an object.
    - -------------------------------------------------------------------------
      If we pass the ``pk`` of the block, together with the ``pk`` of the
      ``Section``, we can get the block model using the ``Section`` fields!!
      So... perhaps we can have a generic view for moving blocks up and down????
      -------------------------------------------------------------------------
    - **TODO** Write a management command for the ``master`` branch which runs
      a check on the ordering.  Output an error message if the order does not
      match the order of the primary key.

    """

    model = PageSection
    form_class = PageSectionOperationForm

    def form_valid(self, form):
        content_pk = self.kwargs["content_pk"]
        operation = form.cleaned_data.get("operation")
        self.object = form.save(commit=False)
        if operation == "down":
            self.object.move_block_down(content_pk)
        elif operation == "up":
            self.object.move_block_up(content_pk)
        elif operation == "ignore":
            pass
        else:
            raise BlockError(
                "A block must be moved 'up' or 'down' ('{}')".format(operation)
            )
        return HttpResponseRedirect(self.object.page.get_design_url())


class ContentCreateView(
    ContentPageMixin, ContentFormMixin, RedirectNextMixin, BaseMixin, CreateView
):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        page_section = self.get_page_section()
        order = self.block_class.objects.next_order(page_section)
        block = self.block_class(page_section=page_section, order=order)
        block.save()
        self.object.block = block
        return super().form_valid(form)

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not url:
            url = super().get_success_url()
        return url


class ContentPublishView(RedirectNextMixin, BaseMixin, UpdateView):
    def form_valid(self, form):
        """Publish 'pending' content."""
        self.object = form.save(commit=False)
        self.object.block.publish(self.request.user)
        messages.info(
            self.request, "Published block, id {}".format(self.object.block.pk)
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not url:
            return self.object.get_design_url()
        return url


class ContentRemoveView(RedirectNextMixin, BaseMixin, UpdateView):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.block.remove(self.request.user)
        messages.info(
            self.request, "Removed block {}".format(self.object.block.pk)
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not url:
            return self.object.get_design_url()
        return url


class ContentUpdateView(
    ContentFormMixin, RedirectNextMixin, BaseMixin, UpdateView
):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_pending_edit()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(is_update=True))
        return context

    def get_section(self):
        return self.object.block.page_section.section

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not url:
            url = self.object.get_design_url()
        return url


class ElementCreateView(BaseMixin, CreateView):
    """Update the parent (content) object so it knows it has been changed."""

    def form_valid(self, form):
        self.object = form.save(commit=False)
        content = self.object.get_parent()
        content.set_pending_edit()
        content.save()
        return super(ElementCreateView, self).form_valid(form)


class ElementDeleteView(BaseMixin, DeleteView):
    """Update the parent (content) object so it knows it has been changed."""

    def delete(self, request, *args, **kwargs):
        with transaction.atomic():
            result = super(ElementDeleteView, self).delete(
                request, *args, **kwargs
            )
            content = self.object.get_parent()
            content.set_pending_edit()
            content.save()
        return result


class ElementUpdateView(BaseMixin, UpdateView):
    """Update the parent (content) object so it knows it has been changed."""

    def form_valid(self, form):
        self.object = form.save(commit=False)
        content = self.object.get_parent()
        content.set_pending_edit()
        content.save()
        return super(ElementUpdateView, self).form_valid(form)


class HeaderFooterUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, UpdateView
):
    form_class = HeaderFooterForm
    model = HeaderFooter

    def get_object(self, queryset=None):
        return HeaderFooter.load()

    def get_success_url(self):
        return reverse("block.page.list")


class MenuMixin(object):
    """Menu - CRUD views."""

    def _get_menu(self):
        return Menu.objects.navigation_menu()


class MenuItemCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, MenuMixin, BaseMixin, CreateView
):
    model = MenuItem
    form_class = MenuItemForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.menu = self._get_menu()
            self.object.slug = slugify(self.object.title)
            self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(menu=self._get_menu()))
        return context

    def get_form_kwargs(self):
        """Returns the argument to any forms on this view."""
        kwargs = super().get_form_kwargs()
        kwargs.update({"menu": self._get_menu()})
        return kwargs

    def get_success_url(self):
        return reverse("block.menuitem.list", args=[self._get_menu().slug])


class MenuItemDeleteView(
    LoginRequiredMixin, CMSRoleRequiredMixin, MenuMixin, BaseMixin, UpdateView
):
    form_class = MenuItemEmptyForm
    model = MenuItem
    template_name = "block/menuitem_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.is_deleted:
            self.object.undelete()
        else:
            self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("block.menuitem.list", args=[self._get_menu().slug])


class MenuItemListView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, ListView
):
    model = MenuItem
    paginate_by = 15

    def get_queryset(self):
        slug = self.kwargs.get("slug", None)
        return MenuItem.objects.menuitem_list(slug)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        menu_slug = self.kwargs.get("slug", None)
        menu = Menu.objects.menu(menu_slug)
        context.update({"menu": menu})
        return context


class MenuItemUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, MenuMixin, BaseMixin, UpdateView
):
    model = MenuItem
    form_class = MenuItemForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        """Returns the argument to any forms on this view."""
        kwargs = super().get_form_kwargs()
        kwargs.update({"menu": self._get_menu(), "this_pk": self.object.pk})
        return kwargs

    def get_success_url(self):
        return reverse("block.menuitem.list", args=[self._get_menu().slug])


class PageCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, CreateView
):
    model = Page

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            if not self.request.user.is_superuser:
                self.object.slug = slugify(self.object.name)
                self.object.order = Page.objects.next_order()
            self.object.save()
            self.object.refresh_sections_from_template()
            Url.objects.init_pages()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_class(self):
        if self.request.user.is_superuser:
            return PageForm
        else:
            return PageFormSimple

    def get_success_url(self):
        return reverse("block.page.list")


class PageDeleteView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, UpdateView
):
    form_class = PageEmptyForm
    model = Page
    template_name = "block/page_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.deleted:
            self.object.deleted = False
        else:
            self.object.deleted = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("block.page.list")


class PageListView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, ListView
):
    model = Page
    paginate_by = 15

    def get_queryset(self):
        return Page.objects.page_list_include_deleted()


class PageTemplateMixin:
    def get_template_names(self):
        page = self.get_page()
        return [page.template.template_name]


class PageDesignMixin:
    def _prepare_sections_design(self, page):
        page_no = self.request.GET.get("page")
        return page.prepare_sections_design(page_no)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = self.get_page()
        view_url = ViewUrl.objects.view_url(
            self.request.user, page, self.request.GET.get("view")
        )
        context.update(dict(design=True, is_block_page=True, view_url=view_url))
        sections = self._prepare_sections_design(page)
        context.update(sections)
        # footer page
        footer = self.get_footer()
        if footer:
            sections = self._prepare_sections_design(footer)
            context.update(sections)
        return context


class PageDesignView(
    LoginRequiredMixin,
    CMSRoleRequiredMixin,
    PageDesignMixin,
    PageTemplateMixin,
    ContentPageMixin,
    TemplateView,
):
    pass


class PageMixin:
    def _check_url(self, page):
        """Check the page is being accessed using the correct URL.

        For custom pages, we make sure the actual URL is different to the
        page URL (we won't have ``custom`` in the URL).  We make an exception
        to this for the home page.

        """
        if self.request.path == page.get_absolute_url():
            if page.is_custom and not page.is_home:
                raise BlockError(
                    "This is a custom page, so the request path "
                    "should NOT match the absolute url: '{}'".format(
                        self.request.path
                    )
                )
        else:
            if page.is_custom:
                pass
            else:
                raise BlockError(
                    "'request.path' ('{}') does not match the absolute url: "
                    "('{}')".format(self.request.path, page.get_absolute_url())
                )

    def _prepare_sections(self, page):
        page_no = self.request.GET.get("page")
        return page.prepare_sections(page_no)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = self.get_page()
        self._check_url(page)
        context.update(dict(design=False, is_block_page=True))
        sections = self._prepare_sections(page)
        context.update(sections)
        # footer page
        footer = self.get_footer()
        if footer:
            sections = self._prepare_sections(footer)
            context.update(sections)
        return context


class PageFormMixin(PageMixin, PageTemplateMixin, ContentPageMixin):
    pass


class PageDetailView(PageFormMixin, DetailView):
    pass


class PageTemplateView(PageFormMixin, TemplateView):
    pass


class PageUpdateView(
    LoginRequiredMixin,
    CMSRoleRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    model = Page

    def get_form_class(self):
        if self.request.user.is_superuser:
            return PageForm
        else:
            return PageFormSimpleUpdate

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        template_category = self.kwargs.get("template_category")
        kwargs.update(dict(template_category=template_category))
        return kwargs

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            self.object.refresh_sections_from_template()
            Url.objects.init_pages()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not url:
            url = reverse("block.page.list")
        return url


class CmsMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                header_footer=HeaderFooter.load(),
                main_menu_item_list=Menu.objects.navigation_menu_items(),
            )
        )
        return context


class CmsPageDesignView(CmsMixin, PageDesignView):
    pass


class CmsPageView(CmsMixin, PageTemplateView):
    pass


class SectionCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):
    form_class = SectionForm
    model = Section

    def get_success_url(self):
        return reverse("block.section.list")


class SectionListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    model = Section
    paginate_by = 15


class SectionUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = SectionForm
    model = Section

    def get_success_url(self):
        return reverse("block.section.list")


class TemplateCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):
    form_class = TemplateForm
    model = Template

    def get_success_url(self):
        return reverse("block.template.list")


class TemplateListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    model = Template
    paginate_by = 15


class TemplateSectionCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):
    form_class = TemplateSectionForm
    model = TemplateSection

    def _get_template(self):
        pk = self.kwargs.get("pk", None)
        template = Template.objects.get(pk=pk)
        return template

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(template=self._get_template()))
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.template = self._get_template()
        with transaction.atomic():
            self.object = form.save()
            # update all the pages with the new sections
            Page.objects.refresh_sections_from_template(self.object.template)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("block.template.list")


class TemplateSectionRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, UpdateView
):
    form_class = TemplateSectionEmptyForm
    model = TemplateSection
    template_name = "block/templatesection_remove_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        with transaction.atomic():
            self.object.delete()
            # remove sections from all existing pages
            Page.objects.refresh_sections_from_template(self.object.template)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("block.template.list")


class TemplateUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = TemplateForm
    model = Template

    def get_success_url(self):
        return reverse("block.template.list")


class WizardLinkMixin(WizardMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kwargs = self._kwargs()
        # categories
        categories = []
        for category in LinkCategory.objects.categories():
            kw = kwargs.copy()
            kw.update({"category": category.slug})
            url = reverse("block.wizard.link.choose", kwargs=kw)
            categories.append(dict(name=category.name, url=url))
        content_obj = self._content_obj()
        context.update(
            dict(
                categories=categories,
                field_name=self._field_name(),
                object=content_obj,
                url_page_design=self._page_design_url(content_obj),
                url_choose=reverse("block.wizard.link.choose", kwargs=kwargs),
                url_external=reverse(
                    "block.wizard.link.external", kwargs=kwargs
                ),
                url_option=reverse("block.wizard.link.option", kwargs=kwargs),
                url_page=reverse("block.wizard.link.page", kwargs=kwargs),
                url_order=reverse("block.wizard.link.order", kwargs=kwargs),
                url_remove=reverse("block.wizard.link.remove", kwargs=kwargs),
                url_select=reverse("block.wizard.link.select", kwargs=kwargs),
                url_upload=reverse("block.wizard.link.upload", kwargs=kwargs),
            )
        )
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            context.update(dict(link=self._get_link()))
        elif link_type == Wizard.MULTI:
            context.update(dict(many_to_many=self._get_many_to_many()))
        else:
            raise BlockError("Unknown 'link_type': '{}'".format(link_type))
        return context

    def _get_link(self):
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            field = self._get_field()
            return field
        else:
            raise BlockError(
                "Cannot '_get_link' for 'link_type': '{}'".format(link_type)
            )

    def _update_link(self, content_obj, link):
        field_name = self._link_field_name(content_obj)
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            setattr(content_obj, field_name, link)
        elif link_type == Wizard.MULTI:
            field = self._get_field()
            class_many_to_many = field.through
            result = class_many_to_many.objects.filter(
                content=content_obj
            ).aggregate(Max("order"))
            order = result.get("order__max") or 0
            order = order + 1
            obj = class_many_to_many(
                content=content_obj, link=link, order=order
            )
            obj.save()
        else:
            raise BlockError("Unknown 'link_type': '{}'".format(link_type))
        content_obj.set_pending_edit()
        content_obj.save()


class WizardMoveMixin:
    """Mixin for moving links or images up and down."""

    form_class = EmptyForm

    def _move_up_down(self, up, down):
        """Move up or down.

        .. note:: This moving logic is also used by ``_move_block_up_down`` in
                  the ``PageSection`` model

        """
        pk = int(up) if up else int(down)
        idx = None
        ordered = []
        many_to_many = self._get_many_to_many()
        for count, item in enumerate(many_to_many):
            if item.pk == pk:
                idx = count
            ordered.append(item.pk)
            count = count + 1
        if idx is None:
            raise BlockError("Cannot find item {} in {}".format(pk, ordered))
        if down:
            if idx == len(ordered) - 1:
                raise BlockError("Cannot move the last item down")
            ordered[idx], ordered[idx + 1] = ordered[idx + 1], ordered[idx]
        elif up:  # up
            if idx == 0:
                raise BlockError("Cannot move the first item up")
            ordered[idx], ordered[idx - 1] = ordered[idx - 1], ordered[idx]
        else:
            raise BlockError("No 'up' or 'down' (why?)")
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            for order, pk in enumerate(ordered, start=1):
                obj = field.through.objects.get(pk=pk, content=content_obj)
                obj.order = order
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def form_valid(self, form):
        up = self.request.POST.get("up")
        down = self.request.POST.get("down")
        if up or down:
            self._move_up_down(up, down)
        return HttpResponseRedirect(self.get_success_url())


class WizardLinkChoose(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, FormView
):
    """Choose a link from the library."""

    template_name = "block/wizard_link_choose.html"

    def _update_links_many_to_many(self, links):
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            field = self._get_field()
            class_many_to_many = field.through
            result = class_many_to_many.objects.filter(
                content=content_obj
            ).aggregate(Max("order"))
            order = result.get("order__max") or 0
            for link in links:
                order = order + 1
                obj = class_many_to_many(
                    content=content_obj, link=link, order=order
                )
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_slug = self.kwargs.get("category")
        category = None
        if category_slug:
            category = LinkCategory.objects.get(slug=category_slug)
        context.update(dict(category=category))
        return context

    def get_form_class(self):
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            return LinkListForm
        elif link_type == Wizard.MULTI:
            return LinkMultiSelectForm
        else:
            raise BlockError("Unknown 'link_type': '{}'".format(link_type))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        category_slug = self.kwargs.get("category")
        kwargs.update(dict(category_slug=category_slug))
        return kwargs

    def form_valid(self, form):
        links = form.cleaned_data["links"]
        content_obj = self._content_obj()
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            self._update_link(content_obj, links)
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            self._update_links_many_to_many(links)
            url = reverse("block.wizard.link.option", kwargs=self._kwargs())
        else:
            raise BlockError("Unknown 'link_type': '{}'".format(link_type))
        return HttpResponseRedirect(url)


class WizardLinkExternal(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, CreateView
):
    """add a link to a url not on this site"""

    form_class = ExternalLinkForm
    template_name = "block/wizard_link_external.html"

    def form_valid(self, form):
        content_obj = self._content_obj()
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.link_type = Link.URL_EXTERNAL
            self.object.save()
            self._update_link(content_obj, self.object)
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            url = reverse("block.wizard.link.option", kwargs=self._kwargs())
        return HttpResponseRedirect(url)


class WizardLinkOption(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, TemplateView
):
    template_name = "block/wizard_link_option.html"


class WizardLinkOrder(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    WizardMoveMixin,
    WizardLinkMixin,
    FormView,
):
    """Set the order of multiple links."""

    template_name = "block/wizard_link_order.html"

    def get_success_url(self):
        return reverse("block.wizard.link.order", kwargs=self._kwargs())


class WizardLinkPage(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, CreateView
):
    """add a link to a page from this site"""

    form_class = PageListForm
    template_name = "block/wizard_link_page.html"

    def form_valid(self, form):
        content_obj = self._content_obj()
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.link_type = Link.URL_INTERNAL
            self.object.save()
            self._update_link(
                content_obj,
                self.object,
                # Link.objects.create_internal_link(self.object)
            )
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            url = reverse("block.wizard.link.option", kwargs=self._kwargs())
        return HttpResponseRedirect(url)


class WizardLinkRemove(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, UpdateView
):
    """remove a link"""

    form_class = ContentEmptyForm
    template_name = "block/wizard_link_remove.html"

    def form_valid(self, form):
        """Set the link on the content object to ``None`` (remove it)."""
        content_obj = self._content_obj()
        self._update_link(content_obj, None)
        return HttpResponseRedirect(self._page_design_url(content_obj))

    def get_context_data(self, **kwargs):
        """Return the current image in the context, so we can display it."""
        context = super().get_context_data(**kwargs)
        field_name = self.kwargs["field"]
        context.update(dict(link=getattr(self.object, field_name)))
        return context

    def get_object(self):
        return self._content_obj()


class WizardLinkUpload(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, CreateView
):
    """Upload a document and link to it"""

    form_class = DocumentForm
    template_name = "block/wizard_link_upload.html"

    def form_valid(self, form):
        category = form.cleaned_data["category"]
        content_obj = self._content_obj()
        with transaction.atomic():
            self.object = form.save()
            self._update_link(
                content_obj,
                Link.objects.create_document_link(self.object, category),
            )
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            url = reverse("block.wizard.link.option", kwargs=self._kwargs())
        return HttpResponseRedirect(url)


class LinkCategoryCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = LinkCategoryForm
    model = LinkCategory

    def get_success_url(self):
        return reverse("block.link.category.list")


class LinkCategoryDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = LinkCategoryEmptyForm
    model = LinkCategory
    template_name = "block/linkcategory_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.in_use:
            raise BlockError(
                "Cannot delete a link category which is "
                "in use: '{}'".format(self.object.slug)
            )
        self.object.deleted = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("block.link.category.list")


class LinkCategoryListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = LinkCategory

    def get_queryset(self):
        return LinkCategory.objects.categories()


class LinkCategoryUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = LinkCategoryForm
    model = LinkCategory

    def get_success_url(self):
        return reverse("block.link.category.list")


class LinkListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    def get_queryset(self):
        return Link.objects.links()


class LinkUpdateMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"url_option": reverse("block.link.list")})
        return context


class LinkDocumentCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, LinkUpdateMixin, CreateView
):
    """Upload a document and link to it"""

    form_class = DocumentForm
    template_name = "block/wizard_link_upload.html"

    def form_valid(self, form):
        category = form.cleaned_data["category"]
        with transaction.atomic():
            self.object = form.save()
            Link.objects.create_document_link(self.object, category)
        url = reverse("block.link.list")
        return HttpResponseRedirect(url)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"in_library": True})
        return kwargs


class LinkDocumentUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, LinkUpdateMixin, UpdateView
):
    """Update a document link

    N.B. A bit more complicated because we need to reference the Link in the
    in the url to the this view but to manage uploading the file the form's
    model is Document
    """

    form_class = DocumentForm
    template_name = "block/wizard_link_upload.html"

    @property
    def _link(self):
        return Link.objects.get(pk=self.kwargs.get("pk"))

    def get_object(self):
        return self._link.document

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"in_library": True})
        return kwargs

    def form_valid(self, form):
        category = form.cleaned_data["category"]
        with transaction.atomic():
            self.object = form.save()
            link = self._link
            link.title = self.object.title
            link.category = category
            link.save()
        return HttpResponseRedirect(reverse("block.link.list"))

    def get_initial(self):
        return {"category": self._link.category}

    def get_success_url(self):
        return reverse("block.link.list")


class LinkUrlExternalUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, LinkUpdateMixin, UpdateView
):
    """add a link to a url not on this site"""

    form_class = ExternalLinkForm
    template_name = "block/wizard_link_external.html"
    model = Link

    def get_success_url(self):
        return reverse("block.link.list")


class LinkUrlInternalUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, LinkUpdateMixin, UpdateView
):
    """update a link to a page from this site"""

    form_class = PageListForm
    template_name = "block/wizard_link_page.html"
    model = Link

    def get_success_url(self):
        return reverse("block.link.list")


class LinkDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = LinkEmptyForm
    model = Link
    template_name = "block/link_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.in_use:
            raise BlockError(
                "Cannot delete a link which is "
                "in use: '{}'".format(self.object.title)
            )
        self.object.deleted = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("block.link.list")


class LinkRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs.get("pk")
        type = kwargs.get("type")
        link = Link.objects.get(pk=pk)
        link = get_object_or_404(Link, pk=pk, link_type=type)
        self.url = link.url
        return super().get_redirect_url(*args, **kwargs)


class WizardLinkSelect(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardLinkMixin, FormView
):
    """List the current links in the slideshow and allow the user to remove.

    Allow the user to de-select any of the images.

    """

    form_class = LinkSelectForm
    template_name = "block/wizard_link_select.html"

    def _update_many_to_many(self, many_to_many):
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            field = self._get_field()
            field.clear()
            class_many_to_many = field.through
            order = 0
            for item in many_to_many:
                order = order + 1
                obj = class_many_to_many(
                    content=content_obj, link=item.link, order=order
                )
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def form_valid(self, form):
        many_to_many = form.cleaned_data["many_to_many"]
        self._update_many_to_many(many_to_many)
        return HttpResponseRedirect(
            reverse("block.wizard.link.option", kwargs=self._kwargs())
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(many_to_many=self._get_many_to_many()))
        return kwargs


class PanelCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):
    block_class = PanelBlock
    form_class = PanelForm
    model = Panel
    template_name = "block/panel_create.html"


class PanelDetailView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, DetailView
):
    model = Panel


class PanelElementCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, CreateView
):
    """Create a panel element.

    A panel element is represented in the database by a Page section with
    the panel element set to the panel block hence the model here is
    PageSection
    """

    model = PageSection
    form_class = PanelElementForm
    template_name = "block/panel_element_form.html"

    def _panel(self):
        return Panel.objects.get(pk=self.kwargs.get("panel"))

    def get_context_data(self):
        context = super().get_context_data()
        context.update({"panel": self._panel()})
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"panel": self._panel()})
        return kwargs

    def get_success_url(self):
        return reverse("block.panel.detail", args=[self._panel().pk])


class PanelElementRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, DeleteView
):
    """Remove a panel element.

    A panel element is represented in the database by a Page section with
    the panel element set to the PanelBlock hence the model here is
    PageSection
    """

    model = PageSection
    form_class = PageSectionEmptyForm
    template_name = "block/panel_element_remove.html"

    def _panel(self):
        return Panel.objects.get(pk=self.kwargs.get("panel"))

    def get_object(self, queryset=None):
        panel = self._panel()
        section_pk = self.kwargs.get("section")
        page = panel.block.page_section.page
        if queryset:
            object = queryset.get(
                page=page, panel=panel.block, section__pk=section_pk
            )
        else:
            object = PageSection.objects.get(
                page=page, panel=panel.block, section__pk=section_pk
            )
        return object

    def get_success_url(self):
        return reverse("block.panel.detail", args=[self._panel().pk])


class PanelPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):
    form_class = ContentEmptyForm
    model = Panel
    template_name = "block/panel_publish.html"

    def form_valid(self, form):
        result = super().form_valid(form)
        if self.object:
            # create a url for any panels with a slug
            self.object.update_urls()
        return result


class PanelRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):
    form_class = ContentEmptyForm
    model = Panel
    template_name = "block/panel_remove.html"


class PanelUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):
    form_class = PanelForm
    model = Panel
    template_name = "block/panel_update.html"
