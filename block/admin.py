# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import BlockSettings, Page, Section


class BlockSettingsAdmin(admin.ModelAdmin):
    pass


admin.site.register(BlockSettings, BlockSettingsAdmin)


class PageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Page, PageAdmin)


class SectionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Section, SectionAdmin)
