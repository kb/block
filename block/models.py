# -*- encoding: utf-8 -*-
import os

from django.apps import apps
from django.conf import settings
from django.contrib.admin.utils import NestedObjects
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models, transaction
from django.db.models import Max
from django.urls import reverse
from django.utils import timezone
from django_extensions.db.fields import AutoSlugField
from reversion import revisions as reversion

from base.model_utils import (
    copy_model_instance,
    TimeStampedModel,
    TimedCreateModifyDeleteModel,
)
from base.singleton import SingletonModel
from gallery.models import Image


def _default_edit_state():
    return EditState.objects._add().pk


def _default_moderate_state():
    return ModerateState.objects._pending().pk


def _paginate_section(qs, page_no, section):
    """Paginate the 'block_list' queryset, using page section properties."""
    if section.paginated:
        # this is the block that requires pagination
        if section.paginated.order_by_field:
            qs = qs.order_by(section.paginated.order_by_field)
        if section.paginated.items_per_page:
            paginator = Paginator(qs, section.paginated.items_per_page)
        try:
            qs = paginator.page(page_no)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            qs = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results
            qs = paginator.page(paginator.num_pages)
    return qs


class Wizard:
    # 'wizard_type'
    IMAGE = "image"
    LINK = "link"

    # 'link_type'
    MULTI = "multi"
    SINGLE = "single"

    def __init__(self, field_name, wizard_type, link_type):
        self.field_name = field_name
        self.wizard_type = wizard_type
        self.link_type = link_type

    @property
    def css_class(self):
        result = ""
        if self.wizard_type == self.IMAGE:
            result = "fa fa-image"
        elif self.wizard_type == self.LINK:
            result = "fa fa-globe"
        else:
            raise BlockError(
                "Unknown wizard type: '{}'".format(self.wizard_type)
            )
        return result

    @property
    def url_name(self):
        result = ""
        if self.wizard_type == self.IMAGE:
            result = "gallery.wizard.image.option"
        elif self.wizard_type == self.LINK:
            result = "block.wizard.link.option"
        else:
            raise BlockError(
                "Unknown wizard type: '{}'".format(self.wizard_type)
            )
        return result


class BlockError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class BlockSettings(SingletonModel):
    can_move_blocks_up_and_down = models.BooleanField(
        default=False,
        help_text=("Allow a member of staff to move blocks up and down"),
    )
    google_site_tag = models.CharField(max_length=50, blank=True)
    design_role = models.ForeignKey(
        Group, blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "Block Settings"
        verbose_name_plural = "Block Settings"

    def __str__(self):
        return "Block Settings: Can move blocks up and down: {}".format(
            self.can_move_blocks_up_and_down
        )


reversion.register(BlockSettings)


class EditStateManager(models.Manager):
    def _add(self):
        """Internal use only."""
        return EditState.objects.get(slug=EditState.ADD)

    def _edit(self):
        """Internal use only."""
        return EditState.objects.get(slug=EditState.EDIT)

    def _push(self):
        """Internal use only."""
        return EditState.objects.get(slug=EditState.PUSH)

    def create_edit_state(self, slug, name):
        obj = self.model(slug=slug, name=name)
        obj.save()
        return obj


class EditState(models.Model):
    """Add, pushed or editing."""

    ADD = "add"
    EDIT = "edit"
    PUSH = "push"

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    objects = EditStateManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Edit state"
        verbose_name_plural = "Edit state"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(EditState)


class ModerateStateManager(models.Manager):
    def _published(self):
        """Internal use only."""
        return self.model.objects.get(slug=ModerateState.PUBLISHED)

    def _pending(self):
        """Internal use only."""
        return self.model.objects.get(slug=ModerateState.PENDING)

    def _removed(self):
        """Internal use only."""
        return self.model.objects.get(slug=ModerateState.REMOVED)

    def _active(self):
        """Internal use only."""
        return self.model.objects.exclude(slug=ModerateState.REMOVED)

    def create_moderate_state(self, slug, name):
        obj = self.model(slug=slug, name=name)
        obj.save()
        return obj


class ModerateState(models.Model):
    """Accept, remove or pending."""

    PENDING = "pending"
    PUBLISHED = "published"
    REMOVED = "removed"

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    objects = ModerateStateManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Moderate"
        verbose_name_plural = "Moderated"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(ModerateState)


class TemplateManager(models.Manager):
    """Move to ``block``?"""

    def create_template(self, name, template_name):
        template = self.model(name=name, template_name=template_name)
        template.save()
        return template

    class Meta:
        verbose_name = "Link"
        verbose_name_plural = "Links"

    def __str__(self):
        return "{}".format(self.title)

    def init_template(self, name, template_name):
        try:
            obj = self.model.objects.get(template_name=template_name)
            obj.name = name
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_template(name, template_name)
        return obj

    def templates(self, category=None):
        if category:
            qs = self.model.objects.filter(category=category)
        else:
            qs = self.model.objects.all()
        return qs.exclude(deleted=True).order_by("name")


class Template(TimeStampedModel):
    name = models.CharField(max_length=100)
    category = models.CharField(
        max_length=100,
        help_text="Optional Category slug (to allow grouping of templates)",
        blank=True,
        null=True,
    )
    template_name = models.CharField(
        max_length=150,
        help_text="File name e.g. 'compose/page_article.html'",
        unique=True,
    )
    deleted = models.BooleanField(default=False)
    objects = TemplateManager()

    class Meta:
        ordering = ("template_name",)
        verbose_name = "Template"
        verbose_name_plural = "Templates"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Template)


class PageManager(models.Manager):
    def create_page(
        self, slug_page, slug_menu, name, order, template, **kwargs
    ):
        obj = self.model(
            name=name,
            slug=slug_page,
            slug_menu=slug_menu,
            order=order,
            template=template,
            is_custom=kwargs.get("is_custom", False),
            is_home=kwargs.get("is_home", False),
        )
        obj.save()
        return obj

    def init_page(self, slug_page, slug_menu, name, order, template, **kwargs):
        if not slug_menu:
            slug_menu = ""
        try:
            obj = Page.objects.get(slug=slug_page, slug_menu=slug_menu)
            obj.name = name
            obj.template = template
            obj.is_custom = kwargs.get("is_custom", False)
            obj.is_home = kwargs.get("is_home", False)
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_page(
                slug_page, slug_menu, name, order, template, **kwargs
            )
        return obj

    def next_order(self):
        result = self.model.objects.aggregate(max_order=Max("order"))
        max_order = result.get("max_order", 0)
        if max_order:
            return max_order + 1
        else:
            return 1

    def menu(self):
        """Return page objects for a menu."""
        return self.pages().exclude(order=0)

    def page_list(self):
        return (
            self.model.objects.all()
            .exclude(deleted=True)
            .order_by("is_custom", "order", "name")
        )

    def page_list_include_deleted(self):
        return self.model.objects.all().order_by(
            "deleted", "is_custom", "order", "name"
        )

    def pages(self):
        """Return all pages (excluding deleted)."""
        return self.page_list().exclude(is_custom=True)

    def refresh_sections_from_template(self, template):
        for p in self.model.objects.filter(template=template):
            p.refresh_sections_from_template()


class Page(TimeStampedModel):
    """A page on the web site.

    slug_menu

      The 'slug_menu' is a extra field that can be used to add a page to a
      sub-menu e.g. 'training/faq'.  In this example, the 'slug_menu' would be
      set to 'faq'.

    order

      An order of zero (0) indicates that the page should be excluded from a
      menu - see 'PageManager', 'menu' (although it doesn't look like it
      excludes items with an order of zero!).

    custom

      A custom page is one where the URL and view have been overridden.  This
      is commonly used to add a form to the page, or add extra context.  Our
      convention is to set the 'slug' to 'Page.CUSTOM' for custom pages.

    """

    CUSTOM = "custom"
    HOME = "home"
    FOOTER = "footer"

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    slug_menu = models.SlugField(max_length=100, blank=True)
    order = models.IntegerField(help_text="Menu order (set to 0 to hide)")
    is_home = models.BooleanField(default=False)
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)
    is_custom = models.BooleanField(default=False)
    meta_description = models.TextField(
        blank=True,
        help_text=(
            "Concise explanation of the contents of this page "
            "(used by search engines - optimal length 155 characters)."
        ),
    )
    meta_keywords = models.TextField(
        blank=True, help_text="keywords for search engines"
    )
    objects = PageManager()

    class Meta:
        ordering = ["order", "slug", "slug_menu"]
        unique_together = ("slug", "slug_menu")
        verbose_name = "Page"
        verbose_name_plural = "Pages"

    def __str__(self):
        return "{}".format(self.name)

    def save(self, *args, **kwargs):
        if not self.slug:
            raise BlockError("Cannot save a page with no slug.")
        # Call the "real" save() method.
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        name = self.url_name
        if self.is_home:
            return reverse(name)
        else:
            return reverse(name, kwargs=self.get_url_kwargs())

    def get_section_queryset(self, page_section, page_number):
        content_model = page_section.section.get_content_model()
        qs = _paginate_section(
            content_model.objects.pending(page_section),
            page_number,
            page_section.section,
        )
        return qs

    def get_design_url(self):
        return reverse("project.page.design", kwargs=self.get_url_kwargs())

    def get_url_kwargs(self):
        result = dict(page=self.slug)
        if self.slug_menu:
            result.update(dict(menu=self.slug_menu))
        return result

    def prepare_sections(self, page_no):
        context = {}
        for e in PageSection.objects.page_level(page=self):
            content_model = e.section.get_content_model()
            qs = _paginate_section(
                content_model.objects.published(e), page_no, e.section
            )
            context.update({"{}_list".format(e.section.slug): qs})
        return context

    def prepare_sections_design(self, page_no):
        context = {}
        for e in PageSection.objects.page_level(page=self):
            qs = self.get_section_queryset(e, page_no)
            context.update({"{}_list".format(e.section.slug): qs})
            create_url = e.section.create_url(self, panel=e.panel)
            if create_url:
                context.update(
                    {"{}_create_url".format(e.section.slug): create_url}
                )
        return context

    def refresh_sections_from_template(self):
        """Update page sections by comparing to the template sections."""
        # iterate through existing sections in the page
        page_level_sections = self.pagesection_set.filter(panel__isnull=True)
        section_slugs = [s.section.slug for s in page_level_sections]
        for slug in section_slugs:
            try:
                # if the section is still used on the page, then keep it.
                template_section = self.template.templatesection_set.get(
                    section__slug=slug
                )
            except TemplateSection.DoesNotExist:
                # pass
                # PJK 30/10/2015
                # Am I too scared to leave this in!!
                # if the section is not used on the page, then delete it.
                PageSection.objects.page_level_section(self, slug).delete()
        # iterate through the new sections
        for template_section in self.template.templatesection_set.all():
            try:
                # if the section exists on the page, then keep it.
                PageSection.objects.page_level_section(
                    page=self, section_slug=template_section.section.slug
                )
            except PageSection.DoesNotExist:
                # if the section is not on the page, then add it.
                page_section = PageSection(
                    page=self, section=template_section.section
                )
                page_section.save()

    def set_deleted(self):
        self.deleted = True
        self.save()

    @property
    def url_name(self):
        """Use by this class and the ``Url`` class (see below)."""
        if self.is_home:
            return "project.home"
        else:
            return "project.page"


reversion.register(Page)


class PaginatedSectionManager(models.Manager):
    def create_paginated_section(self, items_per_page, order_by_field):
        obj = self.model(
            items_per_page=items_per_page, order_by_field=order_by_field
        )
        obj.save()
        return obj

    def init_paginated_section(self, items_per_page, order_by_field):
        try:
            obj = PaginatedSection.objects.get(order_by_field=order_by_field)
            obj.items_per_page = items_per_page
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_paginated_section(items_per_page, order_by_field)
        return obj


class PaginatedSection(models.Model):
    """Parameters for a Paginated Section."""

    items_per_page = models.IntegerField(default=10)
    order_by_field = models.CharField(max_length=100)
    objects = PaginatedSectionManager()

    def __str__(self):
        return "{} - {}".format(self.items_per_page, self.order_by_field)


reversion.register(PaginatedSection)


class SectionManager(models.Manager):
    def create_section(
        self, slug, name, block_app, block_model, create_url_name, **kwargs
    ):
        obj = self.model(
            slug=slug,
            name=name,
            block_app=block_app,
            block_model=block_model,
            create_url_name=create_url_name,
        )
        paginated = kwargs.get("paginated", None)
        obj.snippet_name = kwargs.get("snippet_name", None)
        obj.field_list = kwargs.get("field_list", None)
        if paginated:
            obj.paginated = paginated
        obj.save()
        return obj

    def init_section(
        self, slug, name, block_app, block_model, create_url_name, **kwargs
    ):
        """Create a section if it doesn't already exist."""
        if not create_url_name:
            create_url_name = ""
        try:
            obj = Section.objects.get(slug=slug)
            obj.slug = slug
            obj.name = name
            obj.block_app = block_app
            obj.block_model = block_model
            obj.create_url_name = create_url_name
            obj.paginated = kwargs.get("paginated", None)
            obj.snippet_name = kwargs.get("snippet_name", None)
            obj.field_list = kwargs.get("field_list", None)
            obj.save()
        except self.model.DoesNotExist:
            obj = Section.objects.create_section(
                slug, name, block_app, block_model, create_url_name, **kwargs
            )
        return obj


class Section(TimeStampedModel):
    """Section of the page e.g. content, header, footer."""

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    block_app = models.CharField(
        max_length=100, help_text="app name e.g. 'compose'"
    )
    block_model = models.CharField(
        max_length=100, help_text="model name e.g. 'Article'"
    )
    create_url_name = models.CharField(
        max_length=100,
        blank=True,
        help_text=(
            "url name for creating the model e.g. 'compose.article.create'"
        ),
    )
    paginated = models.ForeignKey(
        PaginatedSection, blank=True, null=True, on_delete=models.CASCADE
    )
    snippet_name = models.CharField(
        max_length=150,
        help_text="Snippet file name e.g. 'compose/_article.html'",
        blank=True,
        null=True,
    )
    field_list = models.CharField(
        max_length=300,
        help_text="Space separated list of field names to use in this section",
        blank=True,
        null=True,
    )
    objects = SectionManager()

    class Meta:
        ordering = ("name",)
        verbose_name = "Section"
        verbose_name_plural = "Sections"

    def __str__(self):
        return "{} ('{}')".format(self.name, self.slug)

    def create_url(self, page, panel=None):
        url = None
        if self.create_url_name:
            kwargs = dict(section=self.slug)
            if panel:
                kwargs.update(panel=panel.id)
            kwargs.update(page.get_url_kwargs())
            url = reverse(self.create_url_name, kwargs=kwargs)
        return url

    def get_content_model(self):
        """Get the content model.

        .. warning:: The ``block_app`` and ``block_model`` fields are actually
                     for the ``ContentModel`` (not the ``BlockModel``).

        """
        if self.block_app and self.block_model:
            content_model = apps.get_model(self.block_app, self.block_model)
            if not content_model:
                raise BlockError(
                    "Content model '{}.{}' does not exist.".format(
                        self.block_app, self.block_model
                    )
                )
        else:
            raise BlockError(
                "The '{}' ('{}') 'Section' does not have the 'block_app' or "
                "'block_model' set.".format(self.slug, self.name)
            )
        return content_model


reversion.register(Section)


class PageSectionManager(models.Manager):
    def create_page_section(self, page, section, panel=None):
        obj = self.model(page=page, section=section, panel=panel)
        obj.save()
        return obj

    def init_page_section(self, page, section, panel=None):
        try:
            if panel:
                obj = self.model.objects.get(
                    page=page, section=section, panel=panel
                )
            else:
                obj = self.model.objects.get(
                    page=page, section=section, panel__isnull=True
                )
        except self.model.DoesNotExist:
            obj = self.create_page_section(page, section, panel)
        return obj

    def page_level(self, page):
        return self.model.objects.filter(page=page, panel__isnull=True)

    def panel_level(self, page, panel):
        return self.model.objects.filter(page=page, panel=panel)

    def page_level_section(self, page, section_slug):
        return self.model.objects.get(
            page=page, section__slug=section_slug, panel__isnull=True
        )


class PageSection(models.Model):
    """Section of a page."""

    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    panel = models.ForeignKey(
        "PanelBlock", blank=True, null=True, on_delete=models.CASCADE
    )
    objects = PageSectionManager()

    class Meta:
        ordering = ("page__slug", "section__slug")
        unique_together = ("page", "section", "panel")
        verbose_name = "Page section"
        verbose_name_plural = "Page sections"

    def __str__(self):
        if self.panel:
            return "{} {} {}".format(
                self.page.name,
                self.panel.page_section.section,
                self.section.name,
            )
        return "{} {}".format(self.page.name, self.section.name)

    def validate_unique(self, exclude=None):
        """override validate_unique to check for null panel."""

        if (
            self.panel is None
            and PageSection.objects.exclude(id=self.id)
            .filter(page=self.page, section=self.section, panel__isnull=True)
            .exists()
        ):
            from django.db import IntegrityError

            raise IntegrityError("Duplicate PageSection")
        super().validate_unique(exclude)

    def save(self, *args, **kwargs):
        """override save to call validate_unique.

        validate_unique is only called when created in a form
        """
        self.validate_unique()
        super().save(*args, **kwargs)

    def _move_block_up_down(self, content_pk, move_down):
        """Move a block up (or down).

        .. note:: A page section instance does not *know* the block or content
                  model.

        1. We find the content model (e.g. ``example_block.Title``) from the
           section using ``block_app`` and ``block_model``.
        2. We get the instance of the content model e.g. ``example_block.Title``
        3. We get the primary key of the block we are going to move.
        4. We find the type of the block model for this content model
           e.g. ``example_block.TitleBlock``.
        5. We get a queryset of all the blocks for this page section.
        6. Then we move the block up or down...

        .. note:: This code is based on the ``_move_up_down`` method from the
                  ``WizardMoveMixin``.

        """
        content_model = self.section.get_content_model()
        content = content_model.objects.get(pk=content_pk)
        # get a list of the pending and published blocks
        qs = content_model.objects.filter(
            block__page_section=self,
            moderate_state__slug__in=[
                ModerateState.PENDING,
                ModerateState.PUBLISHED,
            ],
        )
        current_block_pks = set([x.block.pk for x in qs])
        # get the block model
        block_model = type(content.block)
        block_pk = content.block.pk
        # get the pending and published blocks
        qs = (
            block_model.objects.filter(page_section=self)
            .filter(pk__in=current_block_pks)
            .order_by("order", "pk")
        )
        idx = None
        ordered = []
        for count, item in enumerate(qs):
            if item.pk == block_pk:
                idx = count
            ordered.append(item.pk)
            count = count + 1
        if idx is None:
            raise BlockError(
                "Cannot find block {} in {}".format(block_pk, ordered)
            )
        if move_down:
            if idx == len(ordered) - 1:
                raise BlockError("Cannot move the last block down")
            ordered[idx], ordered[idx + 1] = ordered[idx + 1], ordered[idx]
        else:  # up
            if idx == 0:
                raise BlockError("Cannot move the first block up")
            ordered[idx], ordered[idx - 1] = ordered[idx - 1], ordered[idx]
        with transaction.atomic():
            for order, pk in enumerate(ordered, start=1):
                obj = block_model.objects.get(pk=pk)
                obj.order = order
                obj.save()
        return content

    def move_block_down(self, content_pk):
        self._move_block_up_down(content_pk, move_down=True)

    def move_block_up(self, content_pk):
        self._move_block_up_down(content_pk, move_down=False)


reversion.register(PageSection)


class BlockManager(models.Manager):
    def next_order(self, page_section):
        result = self.model.objects.filter(
            page_section=page_section,
            content__moderate_state__in=ModerateState.objects._active(),
        ).aggregate(Max("order"))
        order = result["order__max"]
        if order is None:
            return 1
        return order + 1


class BlockModel(TimeStampedModel):
    """Abstract base class for blocks of one type."""

    page_section = models.ForeignKey(PageSection, on_delete=models.CASCADE)
    order = models.IntegerField()
    objects = BlockManager()

    class Meta:
        abstract = True
        verbose_name = "Block"
        verbose_name_plural = "Blocks"

    def __str__(self):
        return "{}: page {}, section {}".format(
            self.pk, self.page_section.page.name, self.page_section.section.name
        )

    def _delete_removed_content(self):
        """delete content which was previously removed."""
        try:
            c = self._get_removed()
            c.delete()
        except ObjectDoesNotExist:
            pass

    def _get_removed(self):
        return self.content.get(moderate_state=ModerateState.objects._removed())

    def _remove_published_content(self, user):
        """publishing new content, so remove currently published content."""
        try:
            c = self.get_published()
            c._set_moderated(user, ModerateState.objects._removed())
            c.save()
        except ObjectDoesNotExist:
            pass

    def get_pending(self):
        """If the block has pending content, then get it."""
        return self.content.get(moderate_state=ModerateState.objects._pending())

    def get_published(self):
        """If the block has published content, then get it."""
        return self.content.get(
            moderate_state=ModerateState.objects._published()
        )

    def publish(self, user):
        """Publish content."""
        try:
            pending = self.get_pending()
        except ObjectDoesNotExist:
            raise BlockError("Cannot publish content unless it is 'pending'")
        with transaction.atomic():
            self._delete_removed_content()
            self._remove_published_content(user)
            # copy the pending record to a new published record.
            published_instance = copy_model_instance(pending)
            published_instance._set_moderated(
                user, ModerateState.objects._published()
            )
            published_instance.save()
            # give pending class the opportunity to copy data
            pending.copy_related_data(published_instance)
            # mark the pending record as 'pushed' (published)
            pending.set_pending_pushed()
            pending.save()

    def remove(self, user):
        """Remove content.

        pending only - set pending to removed.
        published only - set published to removed.
        pending and published - delete pending, set published to removed.

        """
        pending = None
        published = None
        try:
            pending = self.get_pending()
        except ObjectDoesNotExist:
            pass
        try:
            published = self.get_published()
        except ObjectDoesNotExist:
            pass
        if not pending and not published:
            raise BlockError(
                "Cannot find pending or published content to remove."
            )
        with transaction.atomic():
            self._delete_removed_content()
            if published:
                published._set_moderated(user, ModerateState.objects._removed())
                published.save()
                if pending:
                    pending.delete()
            else:
                pending._set_moderated(user, ModerateState.objects._removed())
                pending.save()


class ContentManager(models.Manager):
    def pending(self, page_section, kwargs=None):
        """Return a list of pending content for a section.

        Note: we return a list of content instances not a queryset.

        """
        pending = ModerateState.objects._pending()
        qs = self.model.objects.filter(
            block__page_section=page_section, moderate_state=pending
        )
        order_by = None
        if kwargs:
            order_by = kwargs.pop("order_by", None)
            qs = qs.filter(**kwargs)
        if order_by:
            qs = qs.order_by(order_by)
        else:
            qs = qs.order_by("block__order", "block__pk")
        return qs

    def published(self, page_section):
        """Return the published content for a page."""
        return (
            self.model.objects.filter(
                block__page_section=page_section,
                moderate_state__slug=ModerateState.PUBLISHED,
            )
            .select_related("block")
            .select_related("block__page_section")
            .select_related("block__page_section__page")
            .select_related("block__page_section__section")
            .select_related("moderate_state")
            .order_by("block__order", "block__pk")
        )


class ContentModel(TimeStampedModel):
    """Abstract base class for the content within blocks.

    'pushed' is set to 'True' on a 'pending' model when it has just been
    'published'.  When the user edits the 'pending' record, the 'pushed'
    field is set to 'False'.

    """

    moderate_state = models.ForeignKey(
        ModerateState, default=_default_moderate_state, on_delete=models.CASCADE
    )
    date_moderated = models.DateTimeField(blank=True, null=True)
    user_moderated = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    edit_state = models.ForeignKey(
        EditState, default=_default_edit_state, on_delete=models.CASCADE
    )
    objects = ContentManager()

    class Meta:
        abstract = True
        verbose_name = "Block content"
        verbose_name_plural = "Block contents"

    def __str__(self):
        return "{}".format(self.pk)

    def _set_moderated(self, user, moderate_state):
        self.date_moderated = timezone.now()
        self.user_moderated = user
        self.moderate_state = moderate_state

    @property
    def is_pending(self):
        return self.moderate_state.slug == ModerateState.PENDING

    @property
    def is_pending_added(self):
        return self.is_pending and self.edit_state.slug == EditState.ADD

    @property
    def is_pending_edited(self):
        return self.is_pending and self.edit_state.slug == EditState.EDIT

    @property
    def is_pending_pushed(self):
        return self.is_pending and self.edit_state.slug == EditState.PUSH

    @property
    def is_published(self):
        return self.moderate_state.slug == ModerateState.PUBLISHED

    @property
    def is_removed(self):
        return self.moderate_state.slug == ModerateState.REMOVED

    def copy_related_data(self, published_instance):
        """Copy related data from this instance to the published instance.

        If the content type has many to many keys or elements
        e.g. accordion, then override this method to copy the data from
        'pending' to 'published'.

        """
        pass

    def has_elements(self):
        """If the content type has elements e.g. accordion, then override this
        method and return 'True'.
        """
        return False

    def set_pending_edit(self):
        """Content has been edited... so update the state.

        If the content was published, then set the state to 'edit'.
        If the content has never been published ('add'), then leave alone.

        """
        if self.is_pending:
            if self.edit_state == EditState.objects._add():
                pass
            elif self.edit_state == EditState.objects._push():
                self.edit_state = EditState.objects._edit()
        else:
            raise BlockError("Sorry, only pending content can be edited.")

    def get_design_url(self):
        panel_block = self.block.page_section.panel
        panel = panel_block.get_pending() if panel_block else None
        panel_slug = panel.anchor() if panel and panel.slug else ""
        return self.block.page_section.page.get_design_url() + panel_slug

    def set_pending_pushed(self):
        """Pending content is being 'pushed' ('published')."""
        if self.is_pending:
            self.edit_state = EditState.objects._push()
        else:
            raise BlockError("Sorry, only pending content can be edited.")

    def _wizard_url(self, url_name, field_name, wizard_type):
        content_type = ContentType.objects.get_for_model(self)
        return reverse(
            url_name,
            kwargs={
                "content": content_type.pk,
                "pk": self.pk,
                "field": field_name,
                "type": wizard_type,
            },
        )

    @property
    def wizard_urls(self):
        """Return the URLs for the image and link wizards.

        The return value looks like this::
            [
                {
                    'caption': 'Picture',
                    'url': '/gallery/wizard/image/29/1/picture/single/',
                    'class': 'fa fa-image'
                },
                {
                    'caption': 'Link',
                    'url': '/block/wizard/link/29/1/link/single/',
                    'class': 'fa fa-globe'
                }
            ]

        """
        result = []
        if hasattr(self, "wizard_fields"):
            for item in self.wizard_fields:
                result.append(
                    {
                        "caption": item.field_name.title(),
                        "class": item.css_class,
                        "url": self._wizard_url(
                            item.url_name, item.field_name, item.link_type
                        ),
                    }
                )
        return result


class DocumentManager(models.Manager):
    def create_document_from_bytes(self, title, data, file_name):
        obj = self.model(title=title)
        obj.document.save(file_name, ContentFile(data), save=True)
        return obj

    def create_document_from_file_field(self, title, file_field, file_name):
        obj = self.model(title=title)
        new_file = ContentFile(file_field.read())
        new_file.name = file_name
        obj.document = new_file
        obj.save()
        return obj


class Document(models.Model):
    title = models.CharField(max_length=200)
    document = models.FileField(
        upload_to="link/document",
        blank=True,
        null=True,
        help_text="Uploaded document e.g. PDF",
    )
    original_file_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        help_text="Original file name of the document",
    )
    deleted = models.BooleanField(default=False)
    objects = DocumentManager()

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        # unique_together = ('page', 'course')
        verbose_name = "Document"
        verbose_name_plural = "Documents"

    def original_file_path(self):
        """Used by the 'record' app to convert the file to PDF etc."""
        result = None
        try:
            result = os.path.join(
                self.document.field.storage.base_location,
                self.document.file.name,
            )
        except ValueError:
            pass
        return result

    def save(self, *args, **kwargs):
        """Save the original file name."""
        if self.document.name:
            self.original_file_name = os.path.basename(self.document.name)
        # Call the "real" save() method.
        super().save(*args, **kwargs)

    @property
    def url(self):
        return settings.MEDIA_URL + self.document.name


reversion.register(Document)


class HeaderFooter(SingletonModel):
    header = models.CharField(max_length=150, blank=True)
    footer_left = models.TextField(
        blank=True,
        help_text=(
            "A block of text intended to be shown on the "
            "left side of the Footer below a heading "
        ),
    )
    footer_right = models.TextField(
        blank=True,
        help_text=(
            "A block of text intended to be shown on the "
            "right side of the Footer below a heading "
        ),
    )

    url_twitter = models.URLField(verbose_name="Twitter URL", blank=True)
    url_linkedin = models.URLField(verbose_name="LinkedIn URL", blank=True)
    url_facebook = models.URLField(verbose_name="Facebook URL", blank=True)

    # added by tim
    footer_left_header = models.CharField(max_length=150, blank=True)
    footer_right_header = models.CharField(max_length=150, blank=True)

    company_address = models.CharField(max_length=150, blank=True)
    company_phone = models.CharField(max_length=30, blank=True)
    company_fax = models.CharField(max_length=30, blank=True)
    company_email = models.CharField(max_length=30, blank=True)
    company_hours = models.CharField(max_length=150, blank=True)

    google_verification_code = models.CharField(max_length=120, blank=True)
    google_analytics_code = models.CharField(max_length=120, blank=True)
    google_map_long = models.FloatField(default=0)
    google_map_lat = models.FloatField(default=0)
    google_map_zoom = models.PositiveIntegerField(default=17)

    class Meta:
        verbose_name = "Header and footer"
        verbose_name_plural = "Header and footers"

    def __str__(self):
        return "{}".format(self.header)


reversion.register(HeaderFooter)


class LinkCategoryManager(models.Manager):
    def create_category(self, name):
        obj = self.model(name=name)
        obj.save()
        return obj

    def categories(self):
        return self.model.objects.all().exclude(deleted=True).order_by("slug")

    def init_category(self, name):
        count = self.model.objects.filter(name=name).count()
        if not count:
            self.create_category(name)


class LinkCategory(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(max_length=100, unique=True, populate_from=("name",))
    deleted = models.BooleanField(default=False)
    objects = LinkCategoryManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Link Category"
        verbose_name_plural = "Link Categories"

    def __str__(self):
        return "{}".format(self.name)

    @property
    def in_use(self):
        links = Link.objects.filter(category=self, deleted=False)
        return links.count() > 0


reversion.register(LinkCategory)


class UrlManager(models.Manager):
    def init_page_url(self, page, id_on_page=None, alt_title=None):
        """Create a page or page section url."""
        if page.is_custom or page.slug == Page.CUSTOM:
            raise BlockError(
                "Cannot create a URL for a custom "
                "page: '{}'".format(page.name)
            )
        if page.deleted:
            raise BlockError(
                "Cannot create a URL for a deleted "
                "page: '{}'".format(page.name)
            )
        if id_on_page is None:
            id_on_page = ""
        url_title = alt_title if alt_title else page.name
        try:
            obj = self.model.objects.get(page=page, name=id_on_page)
            obj.title = url_title
        except self.model.DoesNotExist:
            obj = self.model(
                title=url_title,
                url_type=self.model.PAGE,
                page=page,
                name=id_on_page,
            )
        obj.save()
        return obj

    def init_pages(self):
        """Add non-custom pages to the list of URLs."""
        for page in Page.objects.pages():
            if not page.is_custom:
                self.init_page_url(page)

    def init_reverse_url(self, title, name, arg1=None, arg2=None, arg3=None):
        arg1 = arg1 or ""
        arg2 = arg2 or ""
        arg3 = arg3 or ""
        try:
            obj = self.model.objects.get(
                name=name, arg1=arg1, arg2=arg2, arg3=arg3
            )
            obj.title = title
        except self.model.DoesNotExist:
            obj = self.model(
                title=title,
                url_type=self.model.REVERSE,
                name=name,
                arg1=arg1,
                arg2=arg2,
                arg3=arg3,
            )
        # check this is a valid url
        obj.url
        obj.save()
        return obj

    def urls(self):
        return self.model.objects.all().exclude(deleted=True).order_by("title")


class Url(models.Model):
    """List of URLs in this project.

    This model stores various types of URL::

    - a page (e.g. / for the home page, /contact/ for the contact page)
    - a page with an id (e.g. /#welcome to link to the <div id="welcome">
      </div> section of the home page) and
    - view URLs (e.g.  ``block.page.list``).

    For the first 2 types (url_type == self.PAGE) the ``page`` field contains a
    foreign key to the page and optionally a section part of the url
    (e.g. ``#welcome``) stored in the ``name`` field.

    For View URLs (url_type == self.REVERSE) the ``page`` field is blank, thes
    url name is stored in the ``name`` field and up to three arguments are
    stored in ``arg1``, ``arg2`` and ``arg3``.

    Some examples of use::

    - To point to the whole contact page (absolute url of /contact/) a link tos
      the contact page is stored in the ``page`` field and the name field is
      an empty string. the url property returns ``/contact/``

    - To point to the <div id="email"> section of a contact page (absolute url
      of ``/contact/``) the ``page`` field stores a link to the contact page
      and the ``name`` field stores ``#email`` the url property returns
      ``/contact/#email``

    - To point to a custom page for with a slug of ``contact`` and slug_menu of
    ``thankyou`` ``name`` fields contains ``project.page``, ``arg1`` contains
    ``contact`` and ``arg2`` contains ``thankyou``.  The url returned would be
    the result of reverse e.g ``/contact/thankyou/``

    In future, we could possibly remove the ``slug`` and ``slug_menu`` from the
    ``Page`` model and allow more flexible partitioning of page urls
    e.g. /department/category/sub-category/item/.

    """

    PAGE = "p"
    REVERSE = "r"

    URL_TYPE_CHOICES = ((PAGE, "Page"), (REVERSE, "Reverse"))

    title = models.CharField(max_length=200)
    url_type = models.CharField(max_length=1, choices=URL_TYPE_CHOICES)
    page = models.ForeignKey(
        Page, blank=True, null=True, on_delete=models.CASCADE
    )
    name = models.CharField(
        max_length=100,
        help_text="e.g. 'project.page' or 'web.training.application'",
    )
    arg1 = models.SlugField(max_length=100, help_text="e.g. 'training'")
    arg2 = models.SlugField(max_length=100, help_text="e.g. 'application'")
    arg3 = models.SlugField(max_length=100, help_text="e.g. 'urgent'")
    deleted = models.BooleanField(default=False)
    objects = UrlManager()

    class Meta:
        unique_together = ("page", "name", "arg1", "arg2", "arg3")
        verbose_name = "URL"
        verbose_name_plural = "URLs"

    def __str__(self):
        return "{}".format(self.title)

    @property
    def url(self):
        result = None
        if self.url_type == self.PAGE:
            result = self.page.get_absolute_url()
            if self.name and self.name[0] == "#":
                result += self.name
        else:
            params = []
            if self.arg1:
                params.append(self.arg1)
            if self.arg2:
                params.append(self.arg2)
            if self.arg3:
                params.append(self.arg3)
            result = reverse(self.name, args=params)
        return result


reversion.register(Url)


class LinkManager(models.Manager):
    def create_document_link(self, document, category=None):
        obj = self.model(
            document=document,
            link_type=self.model.DOCUMENT,
            title=document.title,
        )
        if category:
            obj.category = category
        obj.save()
        return obj

    def create_external_link(self, url_external, title):
        obj = self.model(
            url_external=url_external,
            link_type=self.model.URL_EXTERNAL,
            title=title,
        )
        obj.save()
        return obj

    def create_internal_link(self, url_internal):
        obj = self.model(
            url_internal=url_internal,
            link_type=self.model.URL_INTERNAL,
            title=url_internal.title,
        )
        obj.save()
        return obj

    def links(self):
        """List of links."""
        return (
            self.model.objects.all()
            .exclude(deleted=True)
            .order_by("category__slug", "title")
        )


class Link(TimeStampedModel):
    """A link to something.

    Either:

    - document
    - external url
    - internal url

    For more information, see ``1011-generic-carousel/wip.rst``

    TODO

    - Do we want to add tags field in here so we can search/group links?
      e.g. https://github.com/alex/django-taggit

    """

    # DMS = 'm'
    DOCUMENT = "d"
    URL_INTERNAL = "r"
    URL_EXTERNAL = "u"

    LINK_TYPE_CHOICES = (
        # (DMS, 'Document Management System'),
        (DOCUMENT, "Document"),
        (URL_EXTERNAL, "External URL"),
        (URL_INTERNAL, "Internal URL"),
    )

    title = models.CharField(max_length=250)
    link_type = models.CharField(max_length=1, choices=LINK_TYPE_CHOICES)
    category = models.ForeignKey(
        LinkCategory, blank=True, null=True, on_delete=models.CASCADE
    )
    deleted = models.BooleanField(default=False)

    document = models.ForeignKey(
        Document, blank=True, null=True, on_delete=models.CASCADE
    )
    url_external = models.URLField(
        max_length=512,
        verbose_name="Link",
        blank=True,
        null=True,
        help_text="URL for a web site e.g. http://www.bbc.co.uk/news",
    )
    # dms = GenericKey(help_text="For storing DMS (e.g. Alfresco) documents")
    url_internal = models.ForeignKey(
        Url,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="A page on this web site",
    )
    objects = LinkManager()

    class Meta:
        verbose_name = "Link"
        verbose_name_plural = "Links"

    def __str__(self):
        return "{}".format(self.title)

    def save(self, *args, **kwargs):
        if self.link_type:
            # Call the "real" save() method.
            super().save(*args, **kwargs)
        else:
            raise BlockError("'Link' records must have a 'link_type'")

    @property
    def file_name(self):
        result = None
        if self.link_type == self.DOCUMENT:
            return self.document.original_file_name
        return result

    @property
    def is_document(self):
        return bool(self.link_type == self.DOCUMENT)

    @property
    def is_deleted(self):
        """Is the link deleted?

        PJK 29/04/2019, Currently only implemented for internal links.

        """
        result = False
        if self.is_internal:
            page = self.url_internal.page
            if page:
                result = page.deleted
        return result

    @property
    def is_internal(self):
        return bool(self.link_type == self.URL_INTERNAL)

    @property
    def is_external(self):
        return bool(self.link_type == self.URL_EXTERNAL)

    @property
    def blocks_used(self):
        """Find all models with a foreign key to a link.

        This code looks through all the tables in the database and finds all
        the foreign keys to this link instance (``Link``).

        - Check the StackOverflow article above for more information.
        - ``link_use`` is a row from a model which contains the foreign key.

        See this link for help on NestedObjects (note combine 2 lines for link)
        http://stackoverflow.com/questions/12158714/how-to-show-related-ite
        ms-using-deleteview-in-django

        Basically NestedObjects is a helper util to find objects that have
        defined the items in the list passed to the collect method in a
        foreignkey relationship

        """
        collector = NestedObjects(using="default")
        collector.collect([self])
        usage_list = collector.nested()
        used = []
        if len(usage_list) > 1:
            for link_use in usage_list[1]:
                if hasattr(link_use, "is_published") and hasattr(
                    link_use, "is_pending"
                ):
                    if link_use.is_published or link_use.is_pending:
                        used.append(link_use)
                    elif hasattr(link_use, "deleted") and not link_use.deleted:
                        used.append(link_use)
                elif hasattr(link_use, "deleted"):
                    if not link_use.deleted:
                        used.append(link_use)
        return used

    @property
    def pages_used(self):
        pages = {}
        for section in self.blocks_used:
            if hasattr(section, "block"):
                page = section.block.page_section.page
                pages.update({page.name: page})
            elif hasattr(section, "menu"):
                pages.update({section.menu.slug: section.menu})
        return pages.values()

    @property
    def in_use(self):
        return len(self.blocks_used) > 0

    @property
    def link_type_description(self):
        if self.link_type == self.DOCUMENT:
            return "Document"
        elif self.link_type == self.URL_EXTERNAL:
            return "Web Site"
        elif self.link_type == self.URL_INTERNAL:
            return "Page"
        else:
            raise BlockError(
                "'Link' {} does not have a 'link_type' (or is an "
                "unknown link type: '{}')".format(self.pk, self.link_type)
            )

    @property
    def open_in_tab(self):
        """Should this link be opened in a new tab in the browser?"""
        result = True
        if self.link_type == self.URL_INTERNAL:
            result = False
        return result

    @property
    def url(self):
        result = None
        if self.link_type == self.DOCUMENT:
            return self.document.url
        elif self.link_type == self.URL_EXTERNAL:
            return self.url_external
        elif self.link_type == self.URL_INTERNAL:
            return self.url_internal.url if self.url_internal else None
        else:
            raise BlockError(
                "'Link' {} does not have a 'link_type' (or is an "
                "unknown link type: '{}')".format(self.pk, self.link_type)
            )
        return result


reversion.register(Link)


class TemplateSectionManager(models.Manager):
    """Move to ``block``?"""

    def create_template_section(self, template, section):
        template_section = self.model(template=template, section=section)
        template_section.save()
        return template_section

    def init_template_section(self, template, section):
        try:
            template_section = self.model.objects.get(
                template=template, section=section
            )
        except self.model.DoesNotExist:
            template_section = self.create_template_section(template, section)
        return template_section


class TemplateSection(TimeStampedModel):
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    objects = TemplateSectionManager()

    class Meta:
        ordering = ("template__template_name", "section__name")
        unique_together = ("template", "section")
        verbose_name = "Template section"
        verbose_name_plural = "Template sections"

    def __str__(self):
        return "{}, {}".format(self.template.template_name, self.section.name)


reversion.register(TemplateSection)


class ViewUrlManager(models.Manager):
    def create_view_url(self, user, page, url):
        obj = self.model(user=user, page=page, url=url)
        obj.save()
        return obj

    def view_url(self, user, page, url):
        """Get the view URL for a user and page.

        The view URL is the URL we return to when leaving design mode.

        If we don't pass in a URL, but a URL has already been saved for this
        page, then return it.

        """
        url = url or ""
        if page.is_custom:
            try:
                obj = self.model.objects.get(user=user, page=page)
                if url:
                    obj.url = url
                    obj.save()
                else:
                    url = obj.url
            except self.model.DoesNotExist:
                self.create_view_url(user, page, url)
        else:
            url = page.get_absolute_url()
        return url


class ViewUrl(models.Model):
    """Store the view URL for a custom page."""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    url = models.CharField(max_length=200, blank=True)
    objects = ViewUrlManager()

    class Meta:
        ordering = ("user__username", "page__slug", "page__slug_menu")
        unique_together = ("user", "page")
        verbose_name = "View URL"
        verbose_name_plural = "View URLs"

    def __str__(self):
        return "{} {}".format(self.user.username, self.page.name, self.url)


reversion.register(ViewUrl)


class MenuManager(models.Manager):
    def create_menu(self, slug, title, navigation=True):
        menu = self.model(slug=slug, title=title, navigation=navigation)
        menu.save()
        return menu

    def init_menu(self, slug, title, navigation=None):
        try:
            menu = self.model.objects.get(slug=slug)
            changed = False
            if menu.title != title:
                menu.title = title
                changed = True
            if navigation is not None and menu.navigation != navigation:
                menu.navigation = navigation
                changed = True
            if changed:
                menu.save()
        except self.model.DoesNotExist:
            menu = self.create_menu(
                slug, title, navigation is None or navigation
            )
        return menu

    def menu(self, menu_slug):
        """Return menu - create navigation if does not exist."""
        try:
            menu = Menu.objects.get(slug=menu_slug)
        except Menu.DoesNotExist:
            if menu_slug == Menu.NAVIGATION:
                # Create the default navigation menu
                menu = Menu.objects.create_menu(
                    slug=menu_slug, title="Navigation Menu"
                )
            else:
                menu = None
        return menu

    def navigation_menu(self):
        """Default to menu called 'main' for now."""
        return self.model.objects.get(slug=Menu.NAVIGATION)

    def navigation_menu_items(self):
        """Menu items for the navigation menu."""
        try:
            navigation_menu = self.navigation_menu()
            qs = (
                navigation_menu.menuitem_set.exclude(deleted=True)
                .exclude(parent__isnull=False)
                .select_related("link")
                .select_related("link__url_internal")
                .select_related("link__url_internal__page")
                .select_related("parent")
            )
            # find menu items where the link has been deleted
            pks_to_delete = [x.pk for x in qs if x.link and x.link.is_deleted]
            result = qs.exclude(pk__in=pks_to_delete)
        except self.model.DoesNotExist:
            result = self.model.objects.none()
        return result


class Menu(TimedCreateModifyDeleteModel):
    NAVIGATION = "main"

    slug = models.SlugField(max_length=100, unique=True)
    title = models.CharField(max_length=100)
    navigation = models.BooleanField(default=True)
    objects = MenuManager()

    class Meta:
        ordering = ("navigation", "slug")
        verbose_name = "Menu"
        verbose_name_plural = "Menus"

    def __str__(self):
        return "{}".format(self.title)


reversion.register(Menu)


class MenuItemManager(models.Manager):
    def create_menuitem(self, menu, slug, title, order, link, parent=None):
        menuitem = self.model(
            menu=menu,
            slug=slug,
            title=title,
            order=order,
            link=link,
            parent=parent,
        )
        menuitem.save()
        return menuitem

    def init_menuitem(self, menu, slug, title, order, link, parent=None):
        try:
            menuitem = self.model.objects.get(menu=menu, slug=slug)
            changed = False
            if menuitem.title != title:
                menuitem.title = title
                changed = True
            if menuitem.order != order:
                menuitem.order = order
                changed = True
            if menuitem.link != link:
                menuitem.link = link
                changed = True
            if parent and menuitem.parent != parent:
                menuitem.parent = parent
                changed = True
            if changed:
                menuitem.save()
        except self.model.DoesNotExist:
            menuitem = self.create_menuitem(
                menu, slug, title, order, link, parent
            )
        return menuitem

    def menuitem_list(self, menu_slug):
        """Return queryset with items with no parent first.
         Order of items returned is:
           top level menu items (parent is None)
           Sub menu items (menus ordered as per their order in top level
           menu)
        This method uses queryset.extra to extract the values what have
        a null parent (i.e the top level menu items) first.  The default
        ordering is to display null values at the end.
        """
        menu_items = (
            MenuItem.objects.select_related()
            .filter(menu__slug=menu_slug)
            .order_by("parent", "order", "title")
        )
        qs = menu_items.extra(
            select={"parent_null": "block_menuitem.parent_id is not null"}
        )
        qs = qs.extra(order_by=["parent_null", "parent", "order", "title"])

        return qs

    def parent_list(self, menu, this_pk=None):
        qs = self.model.objects.filter(menu=menu, parent__isnull=True)
        if this_pk:
            qs = qs.exclude(pk=this_pk)
        return qs


class MenuItem(TimedCreateModifyDeleteModel):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=100)
    parent = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.CASCADE
    )
    title = models.CharField(max_length=100)
    order = models.PositiveIntegerField(default=0)
    link = models.ForeignKey(
        Link, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = MenuItemManager()

    class Meta:
        unique_together = ("menu", "slug")
        ordering = ("order", "title")
        verbose_name = "Menu Item"
        verbose_name_plural = "Menu Items"

    def __str__(self):
        result = self.title
        try:
            result = "{} - {}".format(self.menu.title, result)
        except ObjectDoesNotExist:
            pass
        return result

    def children(self):
        """Sub-menu items."""
        qs = self.menuitem_set.exclude(deleted=True)
        # find menu items where the link has been deleted
        pks_to_delete = [x.pk for x in qs if x.link and x.link.is_deleted]
        return qs.exclude(pk__in=pks_to_delete)

    def has_children(self):
        return bool(self.menuitem_set.exclude(deleted=True).count())

    def has_link(self):
        return bool(self.link)

    def get_link(self):
        if self.link:
            return self.link.url
        else:
            return "#"

    def get_content_type(self):
        return ContentType.objects.get_for_model(self)

    def get_design_url(self):
        """Required by the link wizard."""
        return reverse("block.menuitem.list", args=[self.menu.slug])

    def set_pending_edit(self):
        """Required by the link wizard."""
        pass


reversion.register(MenuItem)


class PanelBlock(BlockModel):
    pass


reversion.register(PanelBlock)


from collections import namedtuple


PanelInfo = namedtuple("PanelInfo", ["create_url", "section", "qs"])


class Panel(ContentModel):
    """Panel - allows more flexible layout of page."""

    block = models.ForeignKey(
        PanelBlock, related_name="content", on_delete=models.CASCADE
    )
    slug = models.SlugField(blank=True, null=True, verbose_name="ID on Page")
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=200, blank=True)
    picture = models.ForeignKey(
        Image,
        related_name="panel_picture",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    class Meta:
        # cannot put 'unique_together' on abstract base class
        # https://code.djangoproject.com/ticket/16732
        unique_together = ("block", "moderate_state")
        verbose_name = "Panel"
        verbose_name_plural = "Panels"

    def __str__(self):
        return "{} {}".format(self.name, self.moderate_state)

    def has_elements(self):
        return True

    def url_elements(self):
        return reverse("block.panel.detail", kwargs={"pk": self.pk})

    def url_publish(self):
        return reverse("block.panel.publish", kwargs={"pk": self.pk})

    def url_remove(self):
        return reverse("block.panel.remove", kwargs={"pk": self.pk})

    def url_update(self):
        return reverse("block.panel.update", kwargs={"pk": self.pk})

    def anchor(self):
        return "#{}".format(self.slug) if self.slug else ""

    def get_design_url(self):
        return self.block.page_section.page.get_design_url() + self.anchor()

    def available_sections(self):
        """Return the all unused page sections.

        Also exclude panels.
        """
        page_sections = PageSection.objects.panel_level(
            page=self.block.page_section.page, panel=self.block
        )
        slugs_used = [p.section.slug for p in page_sections]
        return Section.objects.exclude(slug__in=slugs_used).exclude(
            block_app="block", block_model="Panel"
        )

    def elements(self, section_slug=None, page_no=None):
        published = self.moderate_state.slug == ModerateState.PUBLISHED
        # print ("Panel", self.id, ":", self.moderate_state)
        # if section_slug:
        #     return PageSection.objects.filter(
        #         page=self.block.page_section.page,
        #         panel=self,
        #         section__slug=section_slug
        #     )
        # else:
        context = []
        for e in PageSection.objects.panel_level(
            page=self.block.page_section.page, panel=self.block
        ):
            content_model = e.section.get_content_model()
            qs = _paginate_section(
                (
                    content_model.objects.published(e)
                    if published
                    else content_model.objects.pending(e)
                ),
                page_no,
                e.section,
            )
            context.append(
                PanelInfo(
                    e.section.create_url(page=e.page, panel=e.panel),
                    e.section,
                    qs,
                )
            )
        return context

    def update_urls(self):
        """Create/Update the urls for all panels in this page section.

        Todo::
        - Check page for other sections that are Panel or other types of block
        - Check that section slug is unique
        - Delete sections that no longer exist (but not remove manually
            added urls!)
        """
        page_section = self.block.page_section
        for block in page_section.panelblock_set.all():
            try:
                pan = block.get_published()
                if pan and pan.slug:
                    # published and it has a slug - create the url
                    title = pan.title or None
                    id_on_page = "#{}".format(pan.slug)
                    Url.objects.init_page_url(
                        page_section.page, id_on_page, title
                    )
            except Panel.DoesNotExist:
                # no published panels for this block - ignore
                pass

    @property
    def wizard_fields(self):
        return [Wizard("picture", Wizard.IMAGE, Wizard.SINGLE)]


# temporary models (to be deleted in future) -----------------------------------
from taggit.managers import TaggableManager


class ImageCategory(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(max_length=100, unique=True, populate_from=("name",))
    deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ["name"]
        verbose_name = "Image Category"
        verbose_name_plural = "Image Categories"

    def __str__(self):
        return "{}".format(self.name)


class Image(TimeStampedModel):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="link/image")
    original_file_name = models.CharField(max_length=100)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
        help_text="User who uploaded the image",
    )
    deleted = models.BooleanField(default=False)
    category = models.ForeignKey(
        ImageCategory, blank=True, null=True, on_delete=models.CASCADE
    )
    tags = TaggableManager(blank=True, related_name="old_image_tags")

    class Meta:
        verbose_name = "Link Image"
        verbose_name_plural = "Link Images"

    def __str__(self):
        return "{}. {}".format(self.pk, self.title)
