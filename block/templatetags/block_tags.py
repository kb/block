# -*- encoding: utf-8 -*-
from django import template

from block.models import BlockSettings


register = template.Library()


@register.inclusion_tag("block/_add.html")
def block_add(url, caption="", return_path=None):
    return dict(url=url, caption=caption, return_path=return_path)


@register.inclusion_tag("block/_moderate.html")
def block_moderate(
    generic_content,
    can_remove=True,
    caption="",
    return_path=None,
    forloop=None,
    reversed_loop=None,
    legend=None,
    can_move_blocks_up_and_down=None,
):
    settings = BlockSettings.load()
    if can_move_blocks_up_and_down is None:
        can_move_blocks_up_and_down = settings.can_move_blocks_up_and_down
    return dict(
        c=generic_content,
        can_remove=can_remove,
        caption=caption,
        return_path=return_path,
        forloop=forloop,
        reversed_loop=reversed_loop,
        legend=legend,
        can_move_blocks_up_and_down=can_move_blocks_up_and_down,
    )


@register.inclusion_tag("block/_status.html")
def block_status(generic_content):
    return dict(c=generic_content)
