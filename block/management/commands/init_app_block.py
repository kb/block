# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from block.models import Url


class Command(BaseCommand):
    help = "Initialise block application"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        Url.objects.init_pages()
        self.stdout.write("{} - Complete".format(self.help))
