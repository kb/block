# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand

from block.models import ContentModel


class Command(BaseCommand):
    """
    IMPORTANT: Run this under branch master of block and project/beingconverted
    """

    help = "Export all the order values of block.ContentModel"

    def handle(self, *args, **options):
        for sc in ContentModel.__subclasses__():
            with open(sc.__name__, "w", newline="") as csv_file:
                qs = sc.objects.all()
                for row in qs.order_by(
                    "block__page_section__id", "order", "pk"
                ):
                    csv_writer = csv.writer(csv_file)
                    csv_writer.writerow([row.id, row.order])
            print(sc.__name__, "Order Exported")
