# -*- encoding: utf-8 -*-
import csv
from django.core.management.base import BaseCommand
from block.models import ContentModel


class Command(BaseCommand):
    """IMPORTANT:
    Run this under branch 962-move-order-from-content-model-to-block of block
    and project/beingconverted
    """

    help = "Export all the order values of block.ContentModel"

    def handle(self, *args, **options):
        for sc in ContentModel.__subclasses__():
            with open(sc.__name__, newline="") as csvfile:
                order_reader = csv.reader(csvfile, delimiter=",")
                for row in order_reader:
                    print(row[0], row[1])
                    obj = sc.objects.get(id=row[0])
                    obj.block.order = row[1]
                    obj.block.save()
            print(sc.__name__, "Order Import")
