# -*- encoding: utf-8 -*-
import csv

from django.apps import apps
from django.core.management.base import BaseCommand

from block.models import ModerateState, Section


class Command(BaseCommand):
    help = "block - check order #962"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        content_models = set()
        for x in Section.objects.all():
            content_models.add(apps.get_model(x.block_app, x.block_model))
        output = []
        print(
            "{:20} {:20} {:7} {:7} {:5} {:5} {:5}".format(
                "Page slug",
                "Section slug",
                "Content",
                "  Order",
                "",
                "Block",
                "Order",
            )
        )

        for model in content_models:
            qs = model.objects.filter(
                moderate_state__slug=ModerateState.PUBLISHED
            ).order_by("block__page_section", "order", "block__pk")
            previous_page_slug = None
            previous_section_slug = None
            for x in qs:
                page_slug = x.block.page_section.page.slug
                section_slug = x.block.page_section.section.slug
                if (
                    page_slug == previous_page_slug
                    and section_slug == previous_section_slug
                ):
                    pass
                else:
                    order = 0
                    previous_pk = 0
                    previous_page_slug = page_slug
                    previous_section_slug = section_slug
                    print()
                if x.pk > previous_pk:
                    status = ""
                else:
                    status = ":("
                order = order + 1
                previous_pk = x.pk
                print(
                    "{:20} {:20} {:7} {:7} {:5} {:5} {:5}".format(
                        page_slug,
                        section_slug,
                        x.pk,
                        x.order,
                        status,
                        x.block.pk,
                        order,
                    )
                )
                block_model = type(x.block)
                output.append(
                    [
                        block_model._meta.app_label,
                        block_model._meta.object_name,
                        x.block.pk,
                        order,
                    ]
                )
        with open("962-block-export-order.csv", "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            for x in output:
                csv_writer.writerow(x)

        self.stdout.write("{} - Complete".format(self.help))
