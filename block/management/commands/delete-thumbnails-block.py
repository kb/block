# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from easy_thumbnails.files import get_thumbnailer

from block.models import Image, Panel


class Command(BaseCommand):
    help = "'block' - 'delete_thumbnails'"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = 0
        for x in Image.objects.all():
            thumbnailer = get_thumbnailer(x.image)
            thumbnailer.delete_thumbnails()
            count = count + 1
        self.stdout.write(
            "{} - Complete (found {} images)".format(self.help, count)
        )
