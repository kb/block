# -*- encoding: utf-8 -*-
import csv

from django.apps import apps
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "block - import order #962"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        reader = csv.reader(open("962-block-export-order.csv"), "excel-tab")
        print(
            "{:20} {:20} {:10} {:7}".format("app", "model", "block.pk", "order")
        )
        for row in reader:
            block_app, block_model, block_pk, order = row
            print(
                "{:20} {:20} {:10} {:7}".format(
                    block_app, block_model, block_pk, order
                )
            )
            model = apps.get_model(block_app, block_model)
            block = model.objects.get(pk=block_pk)
            block.order = order
            block.save()
        self.stdout.write("{} - Complete".format(self.help))
