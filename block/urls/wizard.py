# -*- encoding: utf-8 -*-
from django.urls import re_path

from block.views import (
    WizardLinkChoose,
    WizardLinkExternal,
    WizardLinkOption,
    WizardLinkOrder,
    WizardLinkPage,
    WizardLinkRemove,
    WizardLinkSelect,
    WizardLinkUpload,
)


urlpatterns = [
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/choose/$",
        view=WizardLinkChoose.as_view(),
        name="block.wizard.link.choose",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/(?P<category>[-\w\d]+)/choose/$",
        view=WizardLinkChoose.as_view(),
        name="block.wizard.link.choose",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/external/$",
        view=WizardLinkExternal.as_view(),
        name="block.wizard.link.external",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/option/$",
        view=WizardLinkOption.as_view(),
        name="block.wizard.link.option",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/order/$",
        view=WizardLinkOrder.as_view(),
        name="block.wizard.link.order",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/remove/$",
        view=WizardLinkRemove.as_view(),
        name="block.wizard.link.remove",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/select/$",
        view=WizardLinkSelect.as_view(),
        name="block.wizard.link.select",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/upload/$",
        view=WizardLinkUpload.as_view(),
        name="block.wizard.link.upload",
    ),
    re_path(
        r"^link/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/page/$",
        view=WizardLinkPage.as_view(),
        name="block.wizard.link.page",
    ),
]
