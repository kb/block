# -*- encoding: utf-8 -*-
from django.urls import re_path

from block.models import Page
from block.views import CmsPageDesignView, CmsPageView


urlpatterns = [
    re_path(
        r"^$",
        view=CmsPageView.as_view(),
        kwargs=dict(page=Page.HOME),
        name="project.home",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/design/$",
        view=CmsPageDesignView.as_view(),
        name="project.page.design",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/design/$",
        view=CmsPageDesignView.as_view(),
        name="project.page.design",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/$",
        view=CmsPageView.as_view(),
        name="project.page",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/$",
        view=CmsPageView.as_view(),
        name="project.page",
    ),
]
