# -*- encoding: utf-8 -*-
from django.db import IntegrityError
from django.urls import reverse

from block.models import BlockError
from block.tests.factories import PageFactory, PageSectionFactory
from login.tests.factories import UserFactory, TEST_PASSWORD


def check_content(model_instance, ignore_remove=None, check_elements=None):
    """Call the standard methods used by the block content.

    An exception will be thrown if the method is not defined
    """
    # check the page methods
    model_instance.block.page_section.page.get_absolute_url()
    # check the standard URLs
    model_instance.url_publish()
    check_urledit(model_instance)

    if model_instance.has_elements():
        model_instance.url_elements()
    if ignore_remove:
        if hasattr(model_instance, "url_remove"):
            raise BlockError(
                "The content model has the 'url_remove' method, but "
                "you have chosen not to test it (see 'ignore_remove' "
                "in 'check_content_methods')"
            )
    else:
        model_instance.url_remove()
    model_instance.url_update()
    # check the order field
    try:
        model_instance.block.order
    except AttributeError:
        raise BlockError(
            "The {} model does not have an 'order' field"
            "".format(model_instance.block.__class__)
        )
    # check the 'content' set
    try:
        model_instance.block.content.all()
    except AttributeError:
        raise BlockError(
            "The 'block' field in the content model does not have "
            "a 'related_name' (should be set to 'content')"
        )
    # check the string method
    str(model_instance)
    # check the unique together constraint
    try:
        model_instance.pk = 0
        model_instance.save()
        raise BlockError(
            "Missing 'unique_together' on {} ("
            "unique_together = ('block', 'moderate_state'))"
            "".format(model_instance.__class__)
        )
    except IntegrityError:
        pass
    # make sure the content model does not have an order field
    try:
        model_instance.order
        raise BlockError(
            "The {} model has an 'order' field. "
            "The 'order' field belongs in the block model."
            "".format(model_instance.__class__)
        )
    except AttributeError:
        pass
    # check the wizard fields
    # try:
    #    model_instance.wizard_fields
    # except AttributeError:
    #    raise BlockError(
    #        "The {} model does not have a 'wizard_fields' property"
    #        "".format(model_instance.__class__)
    #    )


def check_element(model_instance):
    """Call the standard methods used by an element.

    An exception will be thrown if the method is not defined
    """
    # check the element attributes
    model_instance.get_parent()


def check_urledit(model_instance):
    """Call the standard methods used by a model with a url

    An exception will be thrown if the method is not defined
    """
    # check the url attributes
    if "has_url" in dir(model_instance):
        model_instance.get_url_text()
        model_instance.get_url_link()
        model_instance.set_url("", "")
        model_instance.url_urledit()


def check_field_list(client, factory, section, expected_fields):
    """Check field list processing for a block section."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    page = PageFactory(slug_menu="")
    page_section = PageSectionFactory(page=page, section=section)

    response = client.get(
        reverse(
            section.create_url_name,
            kwargs=dict(
                page=page_section.page.slug, section=page_section.section.slug
            ),
        )
    )
    fields = [k for k in response.context["form"].fields.keys()]
    assert fields == expected_fields, "expected fields were {} got {}".format(
        expected_fields, fields
    )

    # create a block instance and assign to the page section
    instance = factory()
    instance.block.page_section = page_section
    instance.block.save()

    response = client.get(instance.url_update())
    fields = [k for k in response.context["form"].fields.keys()]
    assert fields == expected_fields, "expected fields were {} got {}".format(
        expected_fields, fields
    )
