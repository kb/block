# -*- encoding: utf-8 -*-
import pytest

from block.models import BlockSettings


@pytest.mark.django_db
def test_default():
    settings = BlockSettings.load()
    assert settings.can_move_blocks_up_and_down is False


@pytest.mark.django_db
def test_default_then_update():
    settings = BlockSettings.load()
    settings.can_move_blocks_up_and_down = True
    settings.save()
    settings.refresh_from_db()
    assert settings.can_move_blocks_up_and_down is True


@pytest.mark.django_db
def test_str():
    settings = BlockSettings.load()
    assert "Block Settings: Can move blocks up and down: False" == str(settings)
