# -*- encoding: utf-8 -*-
from django.core.management import call_command
from django.test import TestCase

from block.tests.factories import PageFactory


class TestCommand(TestCase):
    def test_demo_data(self):
        """Test the management command"""
        call_command("demo-data-block")

    def test_init_app(self):
        """Test the management command"""
        PageFactory()
        call_command("init_app_block")
