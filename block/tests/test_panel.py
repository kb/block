# -*- encoding: utf-8 -*-
import pytest

from block.tests.helper import check_content
from block.tests.factories import PanelFactory


@pytest.mark.django_db
def test_content_methods():
    c = PanelFactory()
    check_content(c)
