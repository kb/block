# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from block.models import Link, Menu, Url
from block.tests.factories import MenuFactory, MenuItemFactory, PageFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_menu():
    Menu.objects.create_menu(slug=Menu.NAVIGATION, title="Navigation Menu")

    menu = Menu.objects.get(slug=Menu.NAVIGATION)
    assert menu.title == "Navigation Menu"


@pytest.mark.django_db
def test_create_menu_already_exists():
    MenuFactory(slug=Menu.NAVIGATION)
    with pytest.raises(IntegrityError) as e:
        Menu.objects.create_menu(slug=Menu.NAVIGATION, title="Navigation Menu")
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_init_menu_create():
    menu = Menu.objects.init_menu(slug=Menu.NAVIGATION, title="test")
    assert menu.title == "test"
    assert menu.navigation


@pytest.mark.django_db
def test_init_menu_update_not_navigation():
    menu = MenuFactory(slug=Menu.NAVIGATION, title="Nav menu")
    assert menu.title == "Nav menu"
    menu = Menu.objects.init_menu(
        slug=Menu.NAVIGATION, title="test", navigation=False
    )
    assert menu.title == "test"
    assert menu.navigation is False


@pytest.mark.django_db
def test_init_menu_update_is_navigation():
    menu = MenuFactory(slug=Menu.NAVIGATION, title="Nav menu")
    assert menu.title == "Nav menu"
    menu = Menu.objects.init_menu(
        slug=Menu.NAVIGATION, title="test", navigation=True
    )
    assert menu.title == "test"
    assert menu.navigation


@pytest.mark.django_db
def test_navigation_menu_items():
    menu = MenuFactory(slug=Menu.NAVIGATION)
    # a
    MenuItemFactory(menu=menu, slug="a", title="a")
    # b - deleted menu item
    item_b = MenuItemFactory(menu=menu, slug="b", title="b")
    item_b.set_deleted(UserFactory())
    # c
    page_c = PageFactory(slug="page_c", slug_menu="menu_c")
    item_c = MenuItemFactory(menu=menu, slug="c", title="c")
    item_c.link = Link.objects.create_internal_link(
        Url.objects.init_page_url(page_c)
    )
    item_c.save()
    # d - deleted page
    page_d = PageFactory(slug="page_d", slug_menu="menu_d")
    item_d = MenuItemFactory(menu=menu, slug="d", title="d")
    item_d.link = Link.objects.create_internal_link(
        Url.objects.init_page_url(page_d)
    )
    item_d.save()
    page_d.deleted = True
    page_d.save()
    # e
    MenuItemFactory(menu=menu, slug="e", title="e")
    # f
    PageFactory(slug="page_f", slug_menu="menu_f")
    item_f = MenuItemFactory(menu=menu, slug="f", title="f")
    item_f.link = Link.objects.create_internal_link(
        Url.objects.init_reverse_url("Dashboard", "project.dash")
    )
    item_f.save()
    assert ["a", "c", "e", "f"] == [
        m.slug for m in Menu.objects.navigation_menu_items()
    ]
