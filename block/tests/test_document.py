# -*- encoding: utf-8 -*-
import os
import pytest

from django.core.files import File

from block.models import Document
from block.tests.factories import DocumentFactory


@pytest.mark.django_db
def test_create_document_from_bytes():
    data = b"Carrots\n"
    document = Document.objects.create_document_from_bytes(
        "Carrot Cake", data, "carrot.txt"
    )
    document.refresh_from_db()
    assert "carrot" in document.original_file_name
    assert ".txt" in document.original_file_name
    assert "Carrot Cake" == document.title
    data = document.document.read()
    assert b"Carrots\n" == data


@pytest.mark.django_db
def test_create_document_from_file_field():
    test_path_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "test_file.txt"
    )
    with open(test_path_file, "rb") as f:
        django_file = File(f)
        doc = Document(title="Orange")
        doc.document.save("orange.txt", django_file, save=True)
    document = Document.objects.create_document_from_file_field(
        "Apple Pie", doc.document, "apple.txt"
    )
    document.refresh_from_db()
    assert "apple.txt" == document.original_file_name
    assert "Apple Pie" == document.title
    data = document.document.read()
    assert b"Apple and Orange\n" == data


@pytest.mark.django_db
def test_document_factory():
    DocumentFactory()


@pytest.mark.django_db
def test_document_str():
    str(DocumentFactory())


@pytest.mark.django_db
def test_original_file_path():
    document = DocumentFactory()
    assert "/media/link/document/" in document.original_file_path()


@pytest.mark.django_db
def test_original_file_path_no_file():
    document = DocumentFactory(document=None)
    assert document.original_file_path() is None
