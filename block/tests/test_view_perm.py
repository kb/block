# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from block.models import Menu
from block.tests.factories import (
    LinkCategoryFactory,
    LinkFactory,
    MenuFactory,
    MenuItemFactory,
    PageFactory,
    PageSectionFactory,
    PanelFactory,
    SectionFactory,
)
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_create(perm_check):
    perm_check.staff(reverse("block.page.create"))


@pytest.mark.django_db
def test_delete(perm_check):
    page = PageFactory()
    perm_check.staff(reverse("block.page.delete", kwargs=dict(pk=page.pk)))


@pytest.mark.django_db
def test_header_footer_update(perm_check):
    perm_check.staff(reverse("block.header.footer.update"))


@pytest.mark.django_db
def test_list(perm_check):
    perm_check.staff(reverse("block.page.list"))


@pytest.mark.django_db
def test_update(perm_check):
    page = PageFactory()
    perm_check.staff(reverse("block.page.update", kwargs=dict(pk=page.pk)))


@pytest.mark.django_db
def test_link_category(perm_check):
    perm_check.staff(reverse("block.link.category.list"))


@pytest.mark.django_db
def test_link_category_create(perm_check):
    perm_check.staff(reverse("block.link.category.create"))


@pytest.mark.django_db
def test_link_category_delete(perm_check):
    obj = LinkCategoryFactory()
    perm_check.staff(reverse("block.link.category.delete", args=[obj.pk]))


@pytest.mark.django_db
def test_link_category_update(perm_check):
    obj = LinkCategoryFactory()
    perm_check.staff(reverse("block.link.category.update", args=[obj.pk]))


@pytest.mark.django_db
def test_link_document_create(perm_check):
    perm_check.staff(reverse("block.link.document.create"))


@pytest.mark.django_db
def test_link_delete(perm_check):
    obj = LinkFactory()
    perm_check.staff(reverse("block.link.delete", args=[obj.pk]))


@pytest.mark.django_db
def test_link_document_update(perm_check):
    obj = LinkFactory()
    perm_check.staff(reverse("block.link.document.update", args=[obj.pk]))


@pytest.mark.django_db
def test_link_external_update(perm_check):
    obj = LinkFactory()
    perm_check.staff(reverse("block.link.external.update", args=[obj.pk]))


@pytest.mark.django_db
def test_link_internal_update(perm_check):
    obj = LinkFactory()
    perm_check.staff(reverse("block.link.internal.update", args=[obj.pk]))


@pytest.mark.django_db
def test_menu_create(perm_check):
    menu = MenuFactory(slug=Menu.NAVIGATION)
    perm_check.staff(reverse("block.menuitem.create", args=[menu.pk]))


@pytest.mark.django_db
def test_menu_update(perm_check):
    menu = MenuFactory(slug=Menu.NAVIGATION)
    obj = MenuItemFactory(menu=menu)
    perm_check.staff(reverse("block.menuitem.update", args=[obj.pk]))


@pytest.mark.django_db
def test_panel_create(perm_check):
    """test the 2 incarnations of this create url.

    page slug, section slug
    page slug, page slug_menu, section slug
    """
    url_name = "block.panel.create"
    home_page = PageFactory(slug="home", slug_menu="")
    about_page = PageFactory(slug="help", slug_menu="about")
    panel_section = SectionFactory(
        slug="panel", name="Panel", block_app="block", block_model="Panel"
    )
    home_ps = PageSectionFactory(page=home_page, section=panel_section)
    about_ps = PageSectionFactory(page=about_page, section=panel_section)

    url = reverse(
        url_name, kwargs=dict(page=home_page.slug, section=home_ps.section.slug)
    )
    perm_check.staff(url)

    url = reverse(
        url_name,
        kwargs=dict(
            page=about_page.slug,
            menu=about_page.slug_menu,
            section=about_ps.section.slug,
        ),
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_panel_publish(perm_check):
    c = PanelFactory()
    url = reverse("block.panel.publish", kwargs={"pk": c.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_panel_remove(perm_check):
    c = PanelFactory()
    url = reverse("block.panel.remove", kwargs={"pk": c.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_panel_update(perm_check):
    c = PanelFactory()
    url = reverse("block.panel.update", kwargs={"pk": c.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_panel_element_create(perm_check):
    home_page = PageFactory(slug="home")
    panel_section = SectionFactory(
        slug="panel", name="Panel", block_app="block", block_model="Panel"
    )
    PageSectionFactory(page=home_page, section=panel_section)
    panel = PanelFactory()
    url = reverse("block.panel.element.create", kwargs=dict(panel=panel.pk))
    perm_check.staff(url)
