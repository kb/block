# -*- encoding: utf-8 -*-
from block.models import (
    Link,
    LinkCategory,
    Menu,
    MenuItem,
    Page,
    PageSection,
    Section,
    Template,
    TemplateSection,
    Url,
)
from gallery.models import ImageCategory


def get_page_custom_calendar():
    return Page.objects.get(slug=Page.CUSTOM, slug_menu="calendar-information")


def get_page_home():
    return Page.objects.get(slug=Page.HOME)


def get_page_info():
    return Page.objects.get(slug="info")


def get_page_section_home_article():
    return PageSection.objects.get(
        page=get_page_home(), section=get_section_article()
    )


def get_page_section_home_body():
    return PageSection.objects.get(
        page=get_page_home(), section=get_section_body()
    )


def get_page_section_information_body():
    return PageSection.objects.get(
        page=get_page_info(), section=get_section_body()
    )


def get_section_article():
    return Section.objects.get(slug="article")


def get_section_body():
    return Section.objects.get(slug="body")


def get_section_panel():
    return Section.objects.get(slug="panel")


def init_page_link(page):
    page_url = Url.objects.init_page_url(page)
    try:
        link = Link.objects.get(url_internal=page_url)
    except Link.DoesNotExist:
        link = Link.objects.create_internal_link(url_internal=page_url)
    return link


def default_scenario_block():
    # template
    template = Template.objects.init_template("Page", "example/page.html")
    article = Section.objects.init_section(
        "article",
        "Article",
        "example_block",
        "Title",
        "example.title.create",
        snippet_name="example/_article.html",
    )
    body = Section.objects.init_section(
        "body",
        "Body",
        "example_block",
        "Title",
        "example.title.create",
        snippet_name="example/_body.html",
    )
    panel = Section.objects.init_section(
        "panel", "Panel", "block", "Panel", "block.panel.create"
    )
    # template sections
    TemplateSection.objects.init_template_section(template, article)
    TemplateSection.objects.init_template_section(template, body)
    TemplateSection.objects.init_template_section(template, panel)
    # -------------------------------------------------------------------------
    # home page
    home = Page.objects.init_page(
        Page.HOME, "", "Home", 0, template, is_home=True
    )
    # -------------------------------------------------------------------------
    # information page
    info = Page.objects.init_page("info", "", "Info", 1, template)
    # -------------------------------------------------------------------------
    # create sections from template
    Page.objects.refresh_sections_from_template(template)
    # -------------------------------------------------------------------------
    # calendar template
    template = Template.objects.init_template(
        "Calendar", "example/calendar-information.html"
    )
    TemplateSection.objects.init_template_section(template, body)
    # -------------------------------------------------------------------------
    # contact page
    contact = Page.objects.init_page("contact", "", "Contact", 2, template)
    PageSection.objects.init_page_section(contact, body)
    # -------------------------------------------------------------------------
    # calendar page
    calendar = Page.objects.init_page(
        Page.CUSTOM,
        "calendar-information",
        "Calendar",
        3,
        template,
        is_custom=True,
    )
    PageSection.objects.init_page_section(calendar, body)
    # -------------------------------------------------------------------------
    ImageCategory.objects.init_category("Computers")
    ImageCategory.objects.init_category("Fruit")
    LinkCategory.objects.init_category("Contract")
    LinkCategory.objects.init_category("User Guide")

    nav_menu = Menu.objects.init_menu(
        slug=Menu.NAVIGATION, title="Navigation", navigation=True
    )
    MenuItem.objects.init_menuitem(
        nav_menu, "home", "Home", 1, init_page_link(home)
    )
    MenuItem.objects.init_menuitem(
        nav_menu, "info", "Info", 2, init_page_link(info)
    )
    MenuItem.objects.init_menuitem(
        nav_menu, "contact", "Contact", 3, init_page_link(contact)
    )
