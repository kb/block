# -*- encoding: utf-8 -*-
import pytest
from django.db import IntegrityError
from django.test import TestCase

from block.models import PageSection

from block.tests.factories import (
    PageFactory,
    PanelFactory,
    PanelBlockFactory,
    PageSectionFactory,
    SectionFactory,
)


def _setup_panel_tests():
    home = PageFactory(name="Home")
    body_section = SectionFactory(name="Body")
    panel = PanelBlockFactory()
    PanelFactory(name="Panel", block=panel)
    panel_section = SectionFactory(slug="panal", name="panel")
    return home, body_section, panel, panel_section


@pytest.mark.django_db
def test_page_section_validate_unique():
    home, body_section, panel, panel_section = _setup_panel_tests()
    PageSectionFactory(page=home, section=panel_section)

    # check if new row would create a duplicate
    with pytest.raises(IntegrityError):
        new_ps = PageSection(page=home, section=panel_section)
        new_ps.validate_unique()


@pytest.mark.django_db
def test_page_section_panel_items_ignored():
    home, body_section, panel, panel_section = _setup_panel_tests()
    PageSectionFactory(page=home, section=panel_section)

    PageSectionFactory(page=home, section=body_section, panel=panel)

    page_sections = PageSection.objects.page_level(home)
    assert page_sections.count() == 1
    page_section = page_sections.first()
    assert page_section.page == home and page_section.section == panel_section


class TestPageSection(TestCase):
    def setUp(self):
        self.home = PageFactory(name="Home")
        self.body = SectionFactory(name="Body")
        self.panel = PanelFactory(name="Panel")
        SectionFactory(slug="panal", name="panel")

    def test_make_page_section(self):
        PageSectionFactory(page=self.home, section=self.body)

    def test_page_section(self):
        home_body = PageSectionFactory(page=self.home, section=self.body)
        self.assertEqual("Home Body", str(home_body))

    def test_page_section_factory(self):
        PageSectionFactory()

    def test_page_section_duplicate(self):
        PageSectionFactory(page=self.home, section=self.body)
        with self.assertRaises(IntegrityError):
            PageSectionFactory(page=self.home, section=self.body)
