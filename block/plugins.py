# -*- encoding: utf-8 -*-
# Base Plugins for the block app
#
# NavMenuMixin - displays the navigation menu when the staff user hovers over the "VIEW SITE" heading
#
# To use::
#
#   Add block.plugins.NavMenuMixin to the BASE_MIXIN_CONTEXT_PLUGIN in
#   settings/base.py e.g
#
#     BASE_MIXIN_CONTEXT_PLUGIN = ["block.plugins.NavMenuMixin"]
#
#   Add the following to your dash/templates/dash/base.html
#
#     {% block homelink %}
#     {% include 'block/_nav_menu.html' %}
#     {% block homelink %}

from block.models import Menu


class NavMenuMixin:
    def get_context_data(self_for_instance):
        main_menu_items = Menu.objects.navigation_menu_items()

        if main_menu_items:
            return {"main_menu_item_list": main_menu_items}
        return {}
