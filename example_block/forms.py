# -*- encoding: utf-8 -*-
from block.forms import ContentBaseForm

from .models import Title


class TitleForm(ContentBaseForm):
    def __init__(self, *args, **kwargs):
        section_field_list = kwargs.pop("field_list", None)
        super().__init__(*args, **kwargs)
        for name in ("title", "style"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self._setup_fields(section_field_list)

    class Meta:
        model = Title
        fields = ("title", "style")
