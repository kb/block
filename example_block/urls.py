# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, re_path
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from block.models import Page
from block.views import PageDesignView, PageTemplateView
from .views import (
    ExampleView,
    SettingsView,
    TitleCreateView,
    TitlePublishView,
    TitleRemoveView,
    TitleUpdateView,
)


urlpatterns = [
    # '/' send to home
    re_path(
        r"^$",
        view=PageTemplateView.as_view(),
        kwargs=dict(page=Page.HOME),
        name="project.home",
    ),
    re_path(r"^block/", view=include("block.urls.block")),
    re_path(r"^gallery/", view=include("gallery.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^wizard/", view=include("block.urls.wizard")),
    re_path(r"^", view=include("login.urls")),
    # home page when logged in
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(
            url=reverse_lazy("project.home"), permanent=False
        ),
        name="project.dash",
    ),
    # custom page - see https://www.pkimber.net/open/app-block.html
    re_path(
        r"^calendar/information/$",
        view=ExampleView.as_view(),
        kwargs=dict(page=Page.CUSTOM, menu="calendar-information"),
        name="calendar.information",
    ),
    # block page design
    re_path(
        r"^(?P<page>[-\w\d]+)/design/$",
        view=PageDesignView.as_view(),
        name="project.page.design",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/design/$",
        view=PageDesignView.as_view(),
        name="project.page.design",
    ),
    # block page view
    re_path(
        r"^(?P<page>[-\w\d]+)/$",
        view=PageTemplateView.as_view(),
        name="project.page",
    ),
    re_path(
        r"^(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/$",
        view=PageTemplateView.as_view(),
        name="project.page",
    ),
    # title create, publish, update and remove
    re_path(
        r"^title/create/(?P<page>[-\w\d]+)/(?P<panel>\d+)/(?P<section>[-\w\d]+)/$",
        view=TitleCreateView.as_view(),
        name="example.title.create",
    ),
    re_path(
        r"^title/create/(?P<page>[-\w\d]+)/(?P<section>[-\w\d]+)/$",
        view=TitleCreateView.as_view(),
        name="example.title.create",
    ),
    re_path(
        r"^title/create/(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/(?P<section>[-\w\d]+)/$",
        view=TitleCreateView.as_view(),
        name="example.title.create",
    ),
    re_path(
        r"^title/create/(?P<page>[-\w\d]+)/(?P<menu>[-\w\d]+)/((?P<panel>\d+)/?P<section>[-\w\d]+)/$",
        view=TitleCreateView.as_view(),
        name="example.title.create",
    ),
    re_path(
        r"^title/(?P<pk>\d+)/publish/$",
        view=TitlePublishView.as_view(),
        name="example.title.publish",
    ),
    re_path(
        r"^title/(?P<pk>\d+)/update/$",
        view=TitleUpdateView.as_view(),
        name="example.title.update",
    ),
    re_path(
        r"^title/(?P<pk>\d+)/remove/$",
        view=TitleRemoveView.as_view(),
        name="example.title.remove",
    ),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
