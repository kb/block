# -*- encoding: utf-8 -*-
from django.core.management import call_command
from django.test import TestCase


class TestCommand(TestCase):
    def test_demo_data(self):
        """Test the management command"""
        call_command("demo-data-login")
        call_command("demo-data-block")
        call_command("demo-data-example")
