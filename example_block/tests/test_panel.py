# -*- encoding: utf-8 -*-
import pytest

from block.tests.factories import (
    PageFactory,
    PageSectionFactory,
    PanelBlockFactory,
    PanelFactory,
    SectionFactory,
)
from login.tests.fixture import perm_check
from .factories import TitleBlockFactory, TitleFactory


@pytest.mark.django_db
def test_panel_elements(perm_check):
    page = PageFactory(slug="home", slug_menu="", name="Home")
    panel = PanelFactory(
        block=PanelBlockFactory(page_section=PageSectionFactory(page=page))
    )
    section = SectionFactory(
        slug="title",
        name="Title",
        block_app="example_block",
        block_model="Title",
        create_url_name="example.title.create",
    )
    ps = PageSectionFactory(page=page, panel=panel.block, section=section)

    TitleFactory(block=TitleBlockFactory(page_section=ps))
    TitleFactory(block=TitleBlockFactory(page_section=ps))

    es = panel.elements()

    assert len(es) == 1

    assert es[0].section.pk == section.pk
    assert es[0].qs.count() == 2
    assert es[0].create_url == section.create_url(page, panel)


@pytest.mark.django_db
def test_design_url():
    pg = PageFactory(slug="test", slug_menu="")
    s_p = SectionFactory(slug="panel", block_app="block", block_model="Panel")
    s_t = SectionFactory(
        slug="title", block_app="example_block", block_model="Title"
    )
    SectionFactory(
        slug="standalone", block_app="example_block", block_model="Title"
    )
    pnl = PanelFactory(
        slug="the-panel",
        block=PanelBlockFactory(
            page_section=PageSectionFactory(page=pg, section=s_p)
        ),
    )
    ps_p = PageSectionFactory(page=pg, section=s_t, panel=pnl.block)

    ps_s = PageSectionFactory(page=pg, section=s_t)

    t1 = TitleFactory(block=TitleBlockFactory(page_section=ps_p))
    t2 = TitleFactory(block=TitleBlockFactory(page_section=ps_s))

    assert "/test/design/#the-panel" == t1.get_design_url()
    assert "/test/design/#the-panel" == pnl.get_design_url()

    assert "/test/design/" == t2.get_design_url()
