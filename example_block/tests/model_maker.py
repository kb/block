# -*- encoding: utf-8 -*-
from base.tests.model_maker import clean_and_save

from example_block.models import Title, TitleBlock


def make_title_block(page_section, order, **kwargs):
    defaults = dict(page_section=page_section, order=order)
    defaults.update(kwargs)
    return clean_and_save(TitleBlock(**defaults))


def make_title(block, title, **kwargs):
    defaults = dict(block=block, title=title)
    defaults.update(kwargs)
    return clean_and_save(Title(**defaults))
