# -*- encoding: utf-8 -*-
import pytest

from http import HTTPStatus
from django.urls import reverse

from block.models import BlockError, ModerateState
from block.tests.factories import PageSectionFactory, SectionFactory
from example_block.models import Title, TitleBlock
from example_block.tests.factories import TitleBlockFactory, TitleFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_block_move_down(client):
    """Move a block down.

    .. note:: Only pending and published blocks should be moved up and down.
              The title (content) with an order of 6 should be ignored because
              it is removed.

    """
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    TitleFactory(
        block=TitleBlockFactory(page_section=page_section, order=6),
        moderate_state=ModerateState.objects._removed(),
    )
    assert [1, 2, 3, 6] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    content = Title.objects.get(block__order=2)
    response = client.post(
        reverse("block.move.up.down", args=[page_section.pk, content.pk]),
        data={"operation": "down"},
    )
    assert HTTPStatus.FOUND == response.status_code
    assert page_section.page.get_design_url() == response.url
    assert [1, 3, 2, 6] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_block_move_up_down_ignore(client):
    """We use the "ignore" operation for disabled buttons on the form."""
    page_section = PageSectionFactory()
    content = TitleFactory(block=TitleBlockFactory())
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("block.move.up.down", args=[page_section.pk, content.pk]),
        data={"operation": "ignore"},
    )
    assert HTTPStatus.FOUND == response.status_code
    assert page_section.page.get_design_url() == response.url


@pytest.mark.django_db
def test_block_move_up(client):
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    content = Title.objects.get(block__order=2)
    response = client.post(
        reverse("block.move.up.down", args=[page_section.pk, content.pk]),
        data={"operation": "up"},
    )
    assert HTTPStatus.FOUND == response.status_code
    assert page_section.page.get_design_url() == response.url
    assert [2, 1, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_block_move_up_down_invalid_operation(client):
    page_section = PageSectionFactory()
    content = TitleFactory(block=TitleBlockFactory())
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(BlockError) as e:
        client.post(
            reverse("block.move.up.down", args=[page_section.pk, content.pk]),
            data={"operation": "invalid-operation"},
        )
    assert "A block must be moved 'up' or 'down' ('invalid-operation')" in str(
        e.value
    )
