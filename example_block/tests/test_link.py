# -*- encoding: utf-8 -*-
import pytest

from block.models import Link
from block.tests.factories import (
    LinkFactory,
    MenuFactory,
    MenuItemFactory,
    PageFactory,
    PageSectionFactory,
    SectionFactory,
)
from example_block.tests.factories import TitleBlockFactory, TitleFactory


@pytest.mark.django_db
def test_str():
    """This value is used for the select drop down in the form."""
    link = LinkFactory(title="Cricket")
    assert "Cricket" == str(link)


@pytest.mark.django_db
def test_open_in_tab_document():
    link = LinkFactory(link_type=Link.DOCUMENT)
    assert link.open_in_tab is True


@pytest.mark.django_db
def test_open_in_tab_external():
    link = LinkFactory()
    assert link.open_in_tab is True


@pytest.mark.django_db
def test_open_in_tab_internal():
    link = LinkFactory(link_type=Link.URL_INTERNAL)
    assert link.open_in_tab is False


@pytest.mark.django_db
def test_link_blocks_used_title():
    link = LinkFactory(link_type=Link.URL_INTERNAL)
    page = PageFactory(name="PAGE")
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(page=page, section=section)
    title = TitleFactory(
        block=TitleBlockFactory(page_section=page_section), link=link
    )
    link.refresh_from_db()

    assert [b.pk for b in link.blocks_used] == [title.pk]
    assert [str(p) for p in link.pages_used] == [page.name]


@pytest.mark.django_db
def test_link_blocks_used_menu():
    link = LinkFactory(link_type=Link.URL_INTERNAL)
    m = MenuItemFactory(link=link)
    link.refresh_from_db()

    assert [b.pk for b in link.blocks_used] == [m.pk]
    assert [str(p) for p in link.pages_used] == [m.menu.title]


@pytest.mark.django_db
def test_link_blocks_used_menu_and_title():
    link = LinkFactory(link_type=Link.URL_INTERNAL)
    page = PageFactory(name="PAGE")
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(page=page, section=section)
    title = TitleFactory(
        block=TitleBlockFactory(page_section=page_section), link=link
    )
    m = MenuItemFactory(menu=MenuFactory(title="MENU"), link=link)
    link.refresh_from_db()

    assert [b.pk for b in link.blocks_used] == [m.pk, title.pk]
    assert [str(p) for p in link.pages_used] == [m.menu.title, page.name]
