# -*- encoding: utf-8 -*-
import pytest

from block.models import BlockError
from block.tests.factories import PageSectionFactory, SectionFactory
from example_block.models import Title, TitleBlock
from example_block.tests.factories import TitleBlockFactory, TitleFactory


@pytest.mark.django_db
def test_move_block_down():
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    content = Title.objects.get(block__order=2)
    page_section.move_block_down(content.pk)
    assert [1, 3, 2] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_move_block_down_fail_last():
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    content = Title.objects.get(block__order=3)
    with pytest.raises(BlockError) as e:
        page_section.move_block_down(content.pk)
    assert "Cannot move the last block down" in str(e.value)
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_move_block_up():
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    content = Title.objects.get(block__order=2)
    page_section.move_block_up(content.pk)
    assert [2, 1, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_move_block_up_fail_first():
    section = SectionFactory(block_app="example_block", block_model="Title")
    page_section = PageSectionFactory(section=section)
    for order in range(1, 4):
        TitleFactory(
            block=TitleBlockFactory(page_section=page_section, order=order)
        )
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
    content = Title.objects.get(block__order=1)
    with pytest.raises(BlockError) as e:
        page_section.move_block_up(content.pk)
    assert "Cannot move the first block up" in str(e.value)
    assert [1, 2, 3] == [
        x.order for x in TitleBlock.objects.all().order_by("pk")
    ]
