# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import User
from django.urls import reverse

from block.models import ModerateState
from block.tests.helper import check_field_list
from block.tests.factories import (
    PageFactory,
    PageSectionFactory,
    SectionFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory

from example_block.models import Title
from example_block.tests.factories import TitleFactory


def _login(client):
    """login, so we can create, update and publish."""
    try:
        user = User.objects.get(username="staff")
    except User.DoesNotExist:
        user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)


def _create(client, title_text):
    _login(client)
    page = PageFactory(slug_menu="")
    page_section = PageSectionFactory(page=page)
    return client.post(
        reverse(
            "example.title.create",
            kwargs=dict(
                page=page_section.page.slug, section=page_section.section.slug
            ),
        ),
        {"title": title_text},
    )


def _update(client, title, title_text):
    _login(client)
    return client.post(
        reverse("example.title.update", kwargs=dict(pk=title.pk)),
        {"title": title_text},
    )


def _publish(client, title):
    _login(client)
    return client.post(
        reverse("example.title.publish", kwargs=dict(pk=title.pk))
    )


@pytest.mark.django_db
def test_create(client):
    response = _create(client, "title_1")
    assert response.status_code == 302
    title = Title.objects.get(title="title_1")
    assert ModerateState.PENDING == title.moderate_state.slug
    assert title.is_pending_added


@pytest.mark.django_db
def test_create_all_fields(client):
    check_field_list(
        client=client,
        factory=TitleFactory,
        section=SectionFactory(
            block_app="example_block",
            block_model="Title",
            create_url_name="example.title.create",
            field_list="",
        ),
        expected_fields=["title", "style"],
    )


@pytest.mark.django_db
def test_create_title_only(client):
    check_field_list(
        client=client,
        factory=TitleFactory,
        section=SectionFactory(
            block_app="example_block",
            block_model="Title",
            create_url_name="example.title.create",
            field_list="title",
        ),
        expected_fields=["title"],
    )


@pytest.mark.django_db
def test_update(client):
    """Create some content, and then update it."""
    response = _create(client, "title_1")
    assert response.status_code == 302
    title = Title.objects.get(title="title_1")
    assert title.is_pending_added
    # update the title
    response = _update(client, title, "title_2")
    assert response.status_code == 302
    title = Title.objects.get(title="title_2")
    # Is not published, so should be 'pending' not 'pushed'
    assert title.is_pending_added
    # 'title_1' should no longer exist
    with pytest.raises(Title.DoesNotExist):
        Title.objects.get(title="title_1")


@pytest.mark.django_db
def test_publish(client):
    """Create some content, and publish it."""
    _create(client, "title_1")
    title = Title.objects.get(title="title_1")
    response = _publish(client, title)
    assert response.status_code == 302
    # find the pending content
    title = Title.objects.get(
        title="title_1", moderate_state=ModerateState.objects._pending()
    )
    # published, so should be 'pending' and 'pushed'
    assert title.is_pending_pushed
    # find the published content
    title = Title.objects.get(
        title="title_1", moderate_state=ModerateState.objects._published()
    )


@pytest.mark.django_db
def test_publish_update(client):
    # create
    response = _create(client, "title_1")
    assert response.status_code == 302
    # publish
    title = Title.objects.get(title="title_1")
    response = _publish(client, title)
    assert response.status_code == 302
    # Is published, so should be 'pending' and 'pushed'
    title = Title.objects.get(
        title="title_1", moderate_state=ModerateState.objects._pending()
    )
    assert title.is_pending_pushed
    # update
    response = _update(client, title, "title_2")
    assert response.status_code == 302
    # Has been edited, so should be 'pending' not 'pushed'
    title = Title.objects.get(
        title="title_2", moderate_state=ModerateState.objects._pending()
    )
    assert title.is_pending_edited, "state is {} {}".format(
        title.moderate_state.slug, title.edit_state.slug
    )
