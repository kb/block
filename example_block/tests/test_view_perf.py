# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command
from http import HTTPStatus

from block.tests.scenario import get_page_home


@pytest.mark.django_db
def test_page_performance(client, django_assert_num_queries):
    call_command("demo-data-login")
    call_command("demo-data-block")
    call_command("demo-data-example")
    page = get_page_home()
    with django_assert_num_queries(34):
        response = client.get(page.get_absolute_url())
    assert HTTPStatus.OK == response.status_code
