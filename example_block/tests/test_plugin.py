# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from block.models import BlockError
from block.tests.factories import BlockSettingsFactory
from block.tests.scenario import (
    default_scenario_block,
    get_page_custom_calendar,
    get_page_home,
    get_page_info,
)
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_nav_menu_plugin(client):
    """When block.plugins.NavMenuMixin is in the BASE_MIXIN_CONTEXT_PLUGIN
    nav menu should be in dashboard context
    """
    with mock.patch.multiple(
        settings, BASE_MIXIN_CONTEXT_PLUGIN=["block.plugins.NavMenuMixin"]
    ):
        default_scenario_block()

        user = UserFactory(username="staff", is_staff=True)
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # Use settings because "project.dash" is a redirect
        response = client.get(reverse("project.settings"))
        assert 200 == response.status_code
        assert "main_menu_item_list" in response.context
        menu_slugs = [i.slug for i in response.context["main_menu_item_list"]]
        assert ["home", "info", "contact"] == menu_slugs


@pytest.mark.django_db
def test_no_nav_menu_plugin(client):
    """When block.plugins.NavMenuMixin is not in the BASE_MIXIN_CONTEXT_PLUGIN
    nav menu should not be in dashboard context
    """
    with mock.patch.multiple(settings, BASE_MIXIN_CONTEXT_PLUGIN=[]):
        default_scenario_block()

        user = UserFactory(username="staff", is_staff=True)
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # Use settings because "project.dash" is a redirect
        response = client.get(reverse("project.settings"))
        assert 200 == response.status_code
        assert "main_menu_item_list" not in response.context
