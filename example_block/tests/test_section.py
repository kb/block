# -*- encoding: utf-8 -*-
import pytest

from block.models import BlockError
from block.tests.factories import SectionFactory
from example_block.models import Title


@pytest.mark.django_db
def test_str():
    section = SectionFactory(slug="apple", name="Apple")
    assert "Apple ('apple')" == str(section)


@pytest.mark.django_db
def test_get_content_model():
    section = SectionFactory(
        slug="apple",
        name="Apple",
        block_app="example_block",
        block_model="Title",
    )
    assert type(Title()) == section.get_content_model()


@pytest.mark.django_db
def test_get_content_model_no_app_or_model():
    section = SectionFactory(
        slug="apple", name="Apple", block_app="", block_model=""
    )
    with pytest.raises(BlockError) as e:
        section.get_content_model()
    assert (
        "The 'apple' ('Apple') 'Section' does not have "
        "the 'block_app' or 'block_model' set."
    ) in str(e.value)
