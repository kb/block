# -*- encoding: utf-8 -*-
import pytest

from block.tests.factories import PageSectionFactory
from login.tests.factories import UserFactory
from example_block.models import TitleBlock
from .factories import TitleBlockFactory, TitleFactory


@pytest.mark.django_db
def test_block_next_order():
    """Next order increments."""
    block = TitleBlockFactory(order=1)
    TitleFactory(block=block)
    block.publish(UserFactory())
    assert 1 == block.order
    assert 2 == TitleBlock.objects.next_order(block.page_section)


@pytest.mark.django_db
def test_block_next_order_with_removed():
    """Next order increments."""
    block = TitleBlockFactory(order=1)
    TitleFactory(block=block)
    block.publish(UserFactory())
    block.publish(UserFactory())
    assert 1 == block.order
    assert 2 == TitleBlock.objects.next_order(block.page_section)


@pytest.mark.django_db
def test_block_next_order_first_block():
    """No blocks. First `order` is 1"""
    ps = PageSectionFactory()
    assert 1 == TitleBlock.objects.next_order(ps)
