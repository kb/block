# -*- encoding: utf-8 -*-
import pytest

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse

from block.tests.factories import (
    PageFactory,
    PageSectionFactory,
    SectionFactory,
)
from block.tests.helper import check_content
from example_block.models import Title
from example_block.tests.factories import TitleBlockFactory, TitleFactory
from login.tests.factories import UserFactory


def _make_page():
    """
    Page
    Section

    Page + Section

    1. Block (Page + Section) Content (Published) Content (Pending)
    2. Block (Page + Section) Content (Published) Content (Pending)
    3. Block (Page + Section) Content (Published) Content (Pending)

    """
    page = PageFactory(slug="sport", slug_menu="cricket")
    section_1 = SectionFactory(
        slug="headline",
        block_app="example_block",
        block_model="Title",
        create_url_name="example.title.create",
    )
    page_section = PageSectionFactory(page=page, section=section_1)
    # block 1
    block_1 = TitleBlockFactory(page_section=page_section, order=2)
    TitleFactory(block=block_1, title="a")
    block_1.publish(UserFactory())
    # block 2
    block_2 = TitleBlockFactory(page_section=page_section, order=1)
    TitleFactory(block=block_2, title="b")
    block_2.publish(UserFactory())
    # section 2
    section_2 = SectionFactory(
        slug="general",
        block_app="example_block",
        block_model="Title",
        create_url_name="example.title.create",
    )
    page_section = PageSectionFactory(page=page, section=section_2)
    # block 3
    block_3 = TitleBlockFactory(page_section=page_section, order=1)
    TitleFactory(block=block_3, title="c")
    block_3.publish(UserFactory())
    return page


@pytest.mark.django_db
def test_content_methods():
    c = TitleFactory()
    check_content(c)


@pytest.mark.django_db
def test_pending_order():
    """Pending items should be in 'order' order."""
    page_section = PageSectionFactory()
    # block 1
    block_1 = TitleBlockFactory(page_section=page_section, order=5)
    TitleFactory(block=block_1, title="ABC")
    # block 2 (publish)
    block_2 = TitleBlockFactory(page_section=page_section, order=3)
    TitleFactory(block=block_2, title="LMN")
    block_2.publish(UserFactory())
    # block 3 (publish)
    block_3 = TitleBlockFactory(page_section=page_section, order=1)
    TitleFactory(block=block_3, title="XYZ")
    # check order
    assert ["XYZ", "LMN", "ABC"] == [
        t.title for t in Title.objects.pending(page_section)
    ]


@pytest.mark.django_db
def test_prepare_sections(django_assert_num_queries):
    page = _make_page()
    # test
    with django_assert_num_queries(3):
        result = page.prepare_sections(1)
    # check
    assert set(["headline_list", "general_list"]) == set(result.keys())
    qs = result["headline_list"]
    assert ["b", "a"] == [x.title for x in qs]
    qs = result["general_list"]
    assert ["c"] == [x.title for x in qs]


@pytest.mark.django_db
def test_prepare_sections_design(django_assert_num_queries):
    page = _make_page()
    # test
    with django_assert_num_queries(5):
        result = page.prepare_sections_design(1)
    # check
    assert set(
        [
            "general_create_url",
            "general_list",
            "headline_create_url",
            "headline_list",
        ]
    ) == set(result.keys())
    # general
    qs = result["general_list"]
    assert ["c"] == [x.title for x in qs]
    assert (
        "/title/create/sport/cricket/general/" == result["general_create_url"]
    )
    # headline
    qs = result["headline_list"]
    assert ["b", "a"] == [x.title for x in qs]
    qs = result["headline_create_url"]
    assert (
        "/title/create/sport/cricket/headline/" == result["headline_create_url"]
    )


@pytest.mark.django_db
def test_published(django_assert_num_queries):
    """Check we only perform one SQL query."""
    page = PageFactory(slug="home")
    section = SectionFactory(
        slug="article", block_app="example_block", block_model="Title"
    )
    page_section = PageSectionFactory(page=page, section=section)
    for order in range(1, 4):
        block = TitleBlockFactory(page_section=page_section, order=order)
        TitleFactory(block=block, title=str(order))
        block.publish(UserFactory())
    with django_assert_num_queries(1):
        qs = Title.objects.published(page_section)
        assert ["1", "2", "3"] == [x.title for x in qs]
        assert [1, 2, 3] == [x.block.order for x in qs]
        assert 3 == len([x.block.page_section.pk for x in qs])
        assert ["home", "home", "home"] == [
            x.block.page_section.page.slug for x in qs
        ]
        assert ["article", "article", "article"] == [
            x.block.page_section.section.slug for x in qs
        ]
        assert ["published", "published", "published"] == [
            x.moderate_state.slug for x in qs
        ]


@pytest.mark.django_db
def test_published_order():
    """Published items should be in 'order' order."""
    page_section = PageSectionFactory()
    # publish block 1
    block_1 = TitleBlockFactory(page_section=page_section, order=9)
    TitleFactory(block=block_1, title="ABC")
    block_1.publish(UserFactory())
    # publish block 2
    block_2 = TitleBlockFactory(page_section=page_section, order=8)
    TitleFactory(block=block_2, title="XYZ")
    block_2.publish(UserFactory())
    # check order
    assert ["XYZ", "ABC"] == [
        t.title for t in Title.objects.published(page_section)
    ]


@pytest.mark.django_db
def test_wizard_url():
    x = TitleFactory()
    content_type = ContentType.objects.get_for_model(x)
    assert reverse(
        "gallery.wizard.image.option",
        args=[content_type.pk, x.pk, "slideshow", "multi"],
    ) == x._wizard_url(
        "gallery.wizard.image.option",
        "slideshow",
        "multi",
    )
