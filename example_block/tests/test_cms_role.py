# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from block.models import BlockError, BlockSettings
from block.tests.factories import (
    BlockSettingsFactory,
    PageFactory,
    PageSectionFactory,
    SectionFactory,
    TemplateFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory, GroupFactory


def new_title_page_section_factory(page, slug):
    PageSectionFactory(
        page=page,
        section=SectionFactory(
            slug=slug,
            block_app="example_block",
            block_model="Title",
            create_url_name="example.title.create",
        ),
    )


def new_home_page_factory():
    template = TemplateFactory(template_name="example/page.html")
    page = PageFactory(
        template=template, slug="home", slug_menu="", is_home=True
    )
    new_title_page_section_factory(page, "body")
    new_title_page_section_factory(page, "article")
    PageSectionFactory(
        page=page,
        section=SectionFactory(
            slug="panel",
            block_app="block",
            block_model="Panel",
            create_url_name="block.panel.create",
        ),
    )
    return page


@pytest.mark.django_db
def test_cms_role_required_no_settings(client):
    """Allow design if no BlockSettings defined"""
    block_settings = BlockSettings.load()
    assert block_settings.design_role is None

    page = new_home_page_factory()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_no_design_role_defined(client):
    """Allow design if no BlockSettings design_role defined"""
    BlockSettingsFactory()
    block_settings = BlockSettings.load()
    assert block_settings.design_role is None

    page = new_home_page_factory()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_not_logged_in(client):
    """Redirect to login if user not logged in"""
    cms_role = GroupFactory(name="cms")
    BlockSettingsFactory(design_role=cms_role)
    block_settings = BlockSettings.load()
    assert block_settings.design_role == cms_role

    page = new_home_page_factory()
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 302 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_no_role_denied(client):
    """Permission denied if user does not have role"""
    cms_role = GroupFactory(name="cms")
    BlockSettingsFactory(design_role=cms_role)
    block_settings = BlockSettings.load()
    assert block_settings.design_role == cms_role

    page = new_home_page_factory()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 403 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_no_staff_denied(client):
    """Permission denied if user is not staff"""
    cms_role = GroupFactory(name="cms")
    BlockSettingsFactory(design_role=cms_role)
    block_settings = BlockSettings.load()
    assert block_settings.design_role == cms_role

    page = new_home_page_factory()
    user = UserFactory(username="web")
    cms_role.user_set.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    assert user.is_authenticated
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 403 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_has_role_allowed(client):
    """Allow access user has the design role"""
    cms_role = GroupFactory(name="cms")
    BlockSettingsFactory(design_role=cms_role)
    block_settings = BlockSettings.load()
    assert block_settings.design_role == cms_role

    page = new_home_page_factory()
    user = UserFactory(username="staff", is_staff=True)
    cms_role.user_set.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_cms_role_required_superuser_allowed(client):
    """Allow access if user is superuser (and staff) regardless of whether has
    role
    """
    cms_role = GroupFactory(name="cms")
    BlockSettingsFactory(design_role=cms_role)
    block_settings = BlockSettings.load()
    assert block_settings.design_role == cms_role

    page = new_home_page_factory()
    user = UserFactory(username="admin", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("project.page.design", args=[page.slug]))
    assert 200 == response.status_code
