# -*- encoding: utf-8 -*-
from block.tests.scenario import (
    get_page_section_home_article,
    get_page_section_home_body,
    get_page_section_information_body,
)
from login.tests.scenario import get_user_staff

from block.models import ModerateState
from example_block.models import Title
from example_block.tests.model_maker import make_title, make_title_block


def get_block_hatherleigh_old():
    return get_hatherleigh_old().block


def get_block_hatherleigh_three():
    return get_hatherleigh_three().block


def get_block_hatherleigh_two():
    return get_hatherleigh_two().block


def get_hatherleigh_old():
    return Title.objects.get(title="Hatherleigh Old")


def get_hatherleigh_two():
    return Title.objects.get(title="Hatherleigh Two")


def get_hatherleigh_three():
    return Title.objects.get(title="Hatherleigh Three")


def get_jacobstowe_one_pending():
    return Title.objects.get(
        moderate_state__slug=ModerateState.PENDING, title="Jacobstowe One"
    )


def get_jacobstowe_one_published():
    return Title.objects.get(
        moderate_state__slug=ModerateState.PUBLISHED, title="Jacobstowe One"
    )


def get_monkokehampton():
    return Title.objects.get(title="Monkokehampton")


def default_scenario_example():
    """Pages are created in 'block/tests/scenario.py'."""
    # page sections
    home_body = get_page_section_home_body()
    home_article = get_page_section_home_article()
    information_body = get_page_section_information_body()
    # Home, Hatherleigh
    hatherleigh_body_1 = make_title_block(home_body, 1)
    make_title(hatherleigh_body_1, "Hatherleigh Two")
    hatherleigh_body_1.publish(get_user_staff())
    c = hatherleigh_body_1.get_pending()
    c.title = "Hatherleigh Three"
    c.save()
    hatherleigh_body_2 = make_title_block(home_body, 2)
    make_title(hatherleigh_body_2, "Hatherleigh Old")
    hatherleigh_body_2.remove(get_user_staff())
    # Home, Articles (set 'order' to 1 for all blocks to test move up / down)
    for title in ("Apple", "Banana", "Kiwi", "Orange", "Peach", "Strawberry"):
        block = make_title_block(home_article, 1)
        make_title(block, title)
        block.publish(get_user_staff())
    # Home, Jacobstowe
    jacobstowe_body = make_title_block(home_body, 2)
    make_title(jacobstowe_body, "Jacobstowe One")
    jacobstowe_body.publish(get_user_staff())
    # Information, Monkokehampton
    monkokehampton_body = make_title_block(information_body, 1)
    make_title(monkokehampton_body, "Monkokehampton")
    monkokehampton_body.publish(get_user_staff())
