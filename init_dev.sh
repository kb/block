#!/bin/bash

# treat unset variables as an error when substituting.
set -u

# exit immediately if a command exits with a nonzero exit status.
set -e

# pytest --tb=short --show-capture=no -x --create-db

# drop the database - if it exists
psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DATABASE_NAME}"

# create database
psql -X -U postgres -c "CREATE DATABASE ${DATABASE_NAME} TEMPLATE=template0 ENCODING='utf-8';"

django-admin migrate --noinput
django-admin demo_data_login
django-admin demo_data_block
django-admin demo_data_example
django-admin init_app_block
django-admin runserver
